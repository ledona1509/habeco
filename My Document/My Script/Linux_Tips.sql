	iostat -xntd 1 100

-- Lay top ung dung chiem RAM
	ps aux | awk '{print $2, $4, $11, $12}' | sort -k2rn | head -n 40
-- hoac
	top
	(sau do shift + f, order theo memory)

-- Gioi han RAM trong tomcat
	JAVA_OPTS="-Djava.awt.headless=true -Xms256m -Xmx512m -XX:PermSize=64m -XX:MaxPermSize=256m "

-- Cau hinh network server 192.168.1.211
	DNS Viettel ICT: 10.61.11.36
	ect/sysconfig/network-scripts
	xem cau hinh ethenet 0
	lenh restart network linux: service network restart

-- Cai dat gio he thong day du
	date -s "20 MAY 2014 16:55:00"
-- Cai dat ngay thang nam
	date +%Y%m%d -s "20140520"
-- Cai dat gio phut giay
	date +%T -s "16:00:00"

-- Su dung scp de copy du lieu
	scp source_file_name username@destination_host:destination_folder

-- Giai nen file
	tar zxvf filename.tgz
	tar -xzvf file.tar.gz
	tar -xvf file.tar