#@author: NaLD@viettel.com.vn

#Thong so cau hinh
ip=10.30.174.212
port=1521
sid=kunkun
psswdSys=Sys123456
pathDumpdirOri=/home/oracle/dumpdir
nameDumpdirOri=DUMPDIR

echo -n "Default tablespace cua database can Import: "
read defTablespaceOld
echo -n "Dat ten database can tao: "
read userNewDB
echo -n "Dat password cho database can tao: "
read psswdNewDB
echo -n "Nhap file name: "
read userOldDB

# Tao user moi tren database dich
userNewDBCAP=$(echo $userNewDB | tr [a-z] [A-Z])

data_tsp=$userNewDBCAP"_DATA"
index_tsp=$userNewDBCAP"_INDEX"
temp_tsp=$userNewDBCAP"_TEMP"

data_file='/home/oracle/app/oracle/oradata/kunkun/'$userNewDBCAP'_DATA_001.dbf'
index_file='/home/oracle/app/oracle/oradata/kunkun/'$userNewDBCAP'_INDEX_001.dbf'
temp_file='/home/oracle/app/oracle/oradata/kunkun/'$userNewDBCAP'_TEMP_001.dbf'

sqlplus "sys/$psswdSys@(DESCRIPTION=(ADDRESS=(PROTOCOL=TCP)(Host=$ip)(Port=$port))(CONNECT_DATA=(SID=$sid))) AS SYSDBA" <<EOF
  CREATE SMALLFILE 
    TABLESPACE "$data_tsp" 
    NOLOGGING 
    DATAFILE '$data_file' SIZE 5G 
    EXTENT MANAGEMENT LOCAL SEGMENT SPACE MANAGEMENT 
    AUTO;
  EXIT
EOF

sqlplus "sys/$psswdSys@(DESCRIPTION=(ADDRESS=(PROTOCOL=TCP)(Host=$ip)(Port=$port))(CONNECT_DATA=(SID=$sid))) AS SYSDBA" <<EOF
  CREATE SMALLFILE 
    TABLESPACE "$index_tsp" 
    NOLOGGING 
    DATAFILE '$index_file' SIZE 2G
    EXTENT MANAGEMENT LOCAL SEGMENT SPACE MANAGEMENT  
    AUTO;
  EXIT
EOF

sqlplus "sys/$psswdSys@(DESCRIPTION=(ADDRESS=(PROTOCOL=TCP)(Host=$ip)(Port=$port))(CONNECT_DATA=(SID=$sid))) AS SYSDBA" <<EOF
  CREATE SMALLFILE 
    TEMPORARY TABLESPACE "$temp_tsp" 
    TEMPFILE '$temp_file' SIZE 2G 
    EXTENT MANAGEMENT LOCAL
    UNIFORM SIZE 1M;
  EXIT
EOF

sqlplus "sys/$psswdSys@(DESCRIPTION=(ADDRESS=(PROTOCOL=TCP)(Host=$ip)(Port=$port))(CONNECT_DATA=(SID=$sid))) AS SYSDBA" <<EOF
  CREATE USER "$userNewDBCAP" PROFILE "DEFAULT" 
    IDENTIFIED BY "$psswdNewDB" 
    DEFAULT TABLESPACE "$data_tsp" 
    TEMPORARY TABLESPACE "$temp_tsp"
    ACCOUNT UNLOCK;
    GRANT CREATE SYNONYM, CREATE DATABASE LINK TO "$userNewDBCAP";
    GRANT CREATE PROCEDURE TO "$userNewDBCAP";
    GRANT CREATE SEQUENCE TO "$userNewDBCAP";
    GRANT CREATE TABLE TO "$userNewDBCAP";
    GRANT CREATE TRIGGER TO "$userNewDBCAP";
    GRANT CREATE VIEW TO "$userNewDBCAP";
    GRANT CREATE JOB TO "$userNewDBCAP";
    GRANT CREATE SESSION TO "$userNewDBCAP"; 
    GRANT CREATE TYPE TO "$userNewDBCAP"; 
    GRANT CREATE ANY INDEX TO "$userNewDBCAP";
    ALTER USER "$userNewDBCAP" QUOTA UNLIMITED ON "$data_tsp";
    GRANT EXECUTE ON CTX_CLS TO "$userNewDBCAP";
    GRANT EXECUTE ON CTX_DDL TO "$userNewDBCAP";
    GRANT EXECUTE ON CTX_DOC TO "$userNewDBCAP";
    GRANT EXECUTE ON CTX_OUTPUT TO "$userNewDBCAP";
    GRANT EXECUTE ON CTX_QUERY TO "$userNewDBCAP";
    GRANT EXECUTE ON CTX_REPORT TO "$userNewDBCAP";
    GRANT EXECUTE ON CTX_THES TO "$userNewDBCAP";
    grant CREATE ANY MATERIALIZED VIEW TO  "$userNewDBCAP";
    grant select_catalog_role to "$userNewDBCAP";
    grant execute_catalog_role to "$userNewDBCAP";
    grant select any dictionary to "$userNewDBCAP";
    grant execute on dbms_redefinition to "$userNewDBCAP";
    grant debug connect session to "$userNewDBCAP";
    grant debug any procedure to "$userNewDBCAP";
    grant advisor to "$userNewDBCAP"; 

    GRANT read, write ON directory $nameDumpdirOri to system, $userNewDBCAP;
  EXIT
EOF

# import database
cd $pathDumpdirOri
tar -zxvf $userOldDB.tar.gz
impdp $userNewDB/$psswdNewDB DIRECTORY=$nameDumpdirOri DUMPFILE=$userOldDB.dmp REMAP_SCHEMA=$userOldDB:$userNewDB REMAP_TABLESPACE=$defTablespaceOld:$data_tsp transform=segment_attributes:n LOGFILE=$userNewDB.log

echo "TAO DATABASE THANH CONG !!!"
echo "IP: $ip"
echo "SID: $sid"
echo "Database name: $userNewDB"
echo "Password: $psswdNewDB"
