PROCEDURE PRO_HT03_CUS_REWARD (proinfoid NUMBER, procusmapid NUMBER, periodid NUMBER, username STRING)
  IS
    -- Loai 1: Tinh thuong chuong trinh HT03 loai khong co TONG
    -- Xet dieu kien Dat hay Khong dat
    CURSOR c_KTongDK (v_soMuc NUMBER)
    IS
      WITH
      spDK AS(
        SELECT sd.product_id
        FROM pro_info i
        JOIN pro_structure s ON s.pro_info_id = i.pro_info_id
        JOIN pro_structure_detail sd ON sd.pro_structure_id = s.pro_structure_id
        WHERE 1=1
          AND i.pro_info_id = proinfoid AND i.type = 3 AND i.status = 1
          AND s.type IN (1,2)
          AND sd.type = 1 AND sd.min_limit IS NOT NULL AND sd.level_number = v_soMuc
      )
      ,spThoaDK AS(
        SELECT cp.product_id
        FROM pro_cus_process cp
        JOIN pro_cus_map cm ON cm.pro_cus_map_id = cp.pro_cus_map_id
        JOIN pro_cus_map_detail cmd ON cmd.pro_cus_map_id = cm.pro_cus_map_id AND cmd.product_id = cp.product_id
        JOIN pro_structure s ON s.pro_info_id = cm.pro_info_id
        JOIN pro_structure_detail sd ON sd.pro_structure_id = s.pro_structure_id AND sd.product_id = cp.product_id
        WHERE 1=1
          AND cp.pro_cus_map_id = procusmapid AND cp.pro_period_id = periodid
          AND s.type IN (1,2)
          AND cp.quantity >= (cmd.quantity*sd.min_limit/100)
          AND sd.type = 1 AND sd.min_limit IS NOT NULL AND sd.level_number = v_soMuc
          AND cm.level_number = v_soMuc
      )
      SELECT CASE WHEN (SELECT 1 ketQua FROM dual WHERE (SELECT count(1) FROM spDK) = (SELECT count(1) FROM spThoaDK)) IS NULL THEN 0
                  ELSE 1
            END ketQua
      FROM dual;
    
    -- Tinh thuong
    CURSOR c_KTong (v_soMuc NUMBER)
    IS
      SELECT gd.pro_gift_detail_id , cp.product_id 
            ,CASE WHEN cp.quantity <= (cmd.quantity + (cmd.quantity*sd.max_limit/100)) THEN cp.quantity
              ELSE ROUND(cmd.quantity + (cmd.quantity*sd.max_limit/100)) END soLuong
            ,NVL(gd.unit_cost, 0) donGia
      FROM pro_cus_process cp
      JOIN pro_cus_map cm ON cm.pro_cus_map_id = cp.pro_cus_map_id
      JOIN pro_cus_map_detail cmd ON cmd.pro_cus_map_id = cm.pro_cus_map_id AND cmd.product_id = cp.product_id
      JOIN pro_structure s ON s.pro_info_id = cm.pro_info_id
      JOIN pro_structure_detail sd ON sd.pro_structure_id = s.pro_structure_id AND sd.product_id = cp.product_id
      JOIN pro_gift g ON g.pro_structure_id = s.pro_structure_id
      JOIN pro_gift_detail gd ON gd.pro_gift_id = g.pro_gift_id AND gd.product_id = cp.product_id
      WHERE 1=1
        AND cp.pro_cus_map_id = procusmapid AND cp.pro_period_id = periodid AND sd.type = 1 AND sd.level_number = v_soMuc
        AND cm.level_number = v_soMuc AND gd.level_number = v_soMuc;
  
    -- Loai 2: Tinh thuong chuong trinh HT03 co TONG tung SP chua khai bao
    -- Xet dieu kien Dat hay Khong dat
    CURSOR c_TongChuaKBDK (v_soMuc NUMBER)
    IS
      WITH
      tongSP AS(
        SELECT sd.product_id
        FROM pro_info i
        JOIN pro_structure s ON s.pro_info_id = i.pro_info_id
        JOIN pro_structure_detail sd ON sd.pro_structure_id = s.pro_structure_id
        WHERE 1=1
          AND i.pro_info_id = proinfoid AND i.type = 3 AND i.status = 1
          AND s.type IN (2)
          AND sd.type = 1 AND sd.level_number = v_soMuc
      )
      -- MIN_LIMIT IS NOT NULL
      ,spDK AS(
        SELECT sd.product_id
        FROM pro_info i
        JOIN pro_structure s ON s.pro_info_id = i.pro_info_id
        JOIN pro_structure_detail sd ON sd.pro_structure_id = s.pro_structure_id
        WHERE 1=1
          AND i.pro_info_id = proinfoid AND i.type = 3 AND i.status = 1
          AND s.type IN (2)
          AND sd.type = 1 AND sd.min_limit IS NOT NULL AND sd.level_number = v_soMuc
      )
      ,spThoaDK AS(
        SELECT cp.product_id
        FROM pro_cus_process cp
        JOIN pro_cus_map cm ON cm.pro_cus_map_id = cp.pro_cus_map_id
        JOIN pro_cus_map_detail cmd ON cmd.pro_cus_map_id = cm.pro_cus_map_id AND cmd.product_id = cp.product_id
        JOIN pro_structure s ON s.pro_info_id = cm.pro_info_id
        JOIN pro_structure_detail sd ON sd.pro_structure_id = s.pro_structure_id AND sd.product_id = cp.product_id
        WHERE 1=1
          AND cp.pro_cus_map_id = procusmapid AND cp.pro_period_id = periodid
          AND s.type IN (2)
          AND cp.quantity >= (cmd.quantity*sd.min_limit/100)
          AND sd.type = 1 AND sd.min_limit IS NOT NULL AND sd.level_number = v_soMuc
          AND cm.level_number = v_soMuc
      )
      ,dongTong AS (
        SELECT (t1.slDangKy*t2.chanDuoi/100) chanDuoi
        FROM(
          SELECT SUM(cmd.quantity) slDangKy
          FROM pro_cus_map cm
          JOIN pro_cus_map_detail cmd ON cmd.pro_cus_map_id = cm.pro_cus_map_id
          WHERE 1=1
            AND cm.pro_cus_map_id = procusmapid AND cm.level_number = v_soMuc
            AND cmd.product_id NOT IN (SELECT t.product_id FROM spDK t)
            )t1,
            (
          SELECT NVL(sd.min_limit, 0) chanDuoi
          FROM pro_structure s
          JOIN pro_structure_detail sd ON sd.pro_structure_id = s.pro_structure_id
          WHERE 1=1
            AND s.pro_info_id = proinfoid AND s.type IN (2)
            AND sd.type = 2 AND sd.level_number = v_soMuc
            )t2
      )
      ,spKhongDK AS(
        SELECT SUM(t.soLuong) soLuong
        FROM(
          SELECT cp.quantity soLuong
          FROM pro_cus_process cp
          JOIN pro_structure s ON s.pro_info_id = cp.pro_info_id
          JOIN pro_structure_detail sd ON sd.pro_structure_id = s.pro_structure_id AND sd.product_id = cp.product_id
          WHERE 1=1
            AND cp.pro_cus_map_id = procusmapid AND cp.pro_period_id = periodid
            AND cp.product_id NOT IN (SELECT t.product_id FROM spDK t)
            AND sd.type = 1 AND sd.level_number = v_soMuc
            )t
      )
      SELECT CASE WHEN (SELECT 1 ketQua FROM dual WHERE 
                        ((SELECT count(1) FROM spDK) = (SELECT count(1) FROM spThoaDK) AND (SELECT t.soLuong FROM spKhongDK t) >= (SELECT t.chanDuoi FROM dongTong t)
                        OR
                        (SELECT count(1) FROM spDK) = (SELECT count(1) FROM spThoaDK) AND (SELECT count(1) FROM spDK) = (SELECT count(1) FROM tongSP))
                        ) IS NULL THEN 0
                  ELSE 1
            END ketQua
      FROM dual;
    
    -- Tinh thuong
    CURSOR c_TongChuaKB (v_soMuc NUMBER)
    IS
      WITH
      -- MAX_LIMIT IS NULL
      dsSPChuaKBMax AS(
        SELECT sd.product_id
        FROM pro_info i
        JOIN pro_structure s ON s.pro_info_id = i.pro_info_id
        JOIN pro_structure_detail sd ON sd.pro_structure_id = s.pro_structure_id
        WHERE 1=1
          AND i.pro_info_id = proinfoid AND i.type = 3 AND i.status = 1
          AND s.type IN (2)
          AND sd.type = 1 AND sd.max_limit IS NULL AND sd.level_number = v_soMuc
      )
      -- Tinh thuong cho nhung san pham co khai bao (max_limit NOT NULL)
      ,spKhaiBao AS(
        SELECT gd.pro_gift_detail_id, cp.product_id
              ,CASE WHEN cp.quantity <= (cmd.quantity + (cmd.quantity*sd.max_limit/100)) THEN cp.quantity
              ELSE ROUND(cmd.quantity + (cmd.quantity*sd.max_limit/100)) END soLuong
              , NVL(gd.unit_cost, 0) donGia
        FROM pro_cus_process cp
        JOIN pro_cus_map cm ON cm.pro_cus_map_id = cp.pro_cus_map_id
        JOIN pro_cus_map_detail cmd ON cmd.pro_cus_map_id = cm.pro_cus_map_id AND cmd.product_id = cp.product_id
        JOIN pro_structure s ON s.pro_info_id = cm.pro_info_id
        JOIN pro_structure_detail sd ON sd.pro_structure_id = s.pro_structure_id AND sd.product_id = cp.product_id
        JOIN pro_gift g ON g.pro_structure_id = s.pro_structure_id
        JOIN pro_gift_detail gd ON gd.pro_gift_id = g.pro_gift_id AND gd.product_id = cp.product_id
        WHERE 1=1
          AND cp.pro_cus_map_id = procusmapid AND cp.pro_period_id = periodid
          AND sd.max_limit IS NOT NULL AND sd.type = 1 AND sd.level_number = v_soMuc
          AND cm.level_number = v_soMuc AND gd.level_number = v_soMuc
      )
      -- Tinh thuong cho nhung san pham chua khai bao (max_limit IS NULL)
      ,spChuaKhaiBao AS(
        SELECT t.pro_gift_detail_id, NULL product_id
              ,CASE WHEN t.slThucHien <= (t.slDangKy + t.slDangKy*t.max_limit/100) THEN t.slThucHien
              ELSE ROUND(t.slDangKy + t.slDangKy*t.max_limit/100) END soLuong
              ,t.donGia
        FROM(  
          -- Chi co 1 dong
          SELECT gd.pro_gift_detail_id, sd.min_limit, sd.max_limit
                ,NVL((SELECT SUM(cp.quantity) FROM pro_cus_process cp WHERE cp.pro_cus_map_id = procusmapid AND cp.pro_period_id = periodid
                  AND cp.product_id IN (SELECT t.product_id FROM dsSPChuaKBMax t)), 0) slThucHien
                , (SELECT SUM(cmd.quantity) FROM pro_cus_map cm
                  JOIN pro_cus_map_detail cmd ON cm.pro_cus_map_id = cmd.pro_cus_map_id
                  WHERE cm.pro_cus_map_id = procusmapid AND cm.level_number = v_soMuc AND cmd.product_id IN (SELECT t.product_id FROM dsSPChuaKBMax t)) slDangKy
                ,NVL(gd.unit_cost, 0) donGia
          FROM pro_info i
          JOIN pro_structure s ON s.pro_info_id = i.pro_info_id
          JOIN pro_structure_detail sd ON sd.pro_structure_id = s.pro_structure_id
          JOIN pro_gift g ON g.pro_structure_id = s.pro_structure_id
          JOIN pro_gift_detail gd ON gd.pro_gift_id = g.pro_gift_id
          WHERE 1=1
            AND i.pro_info_id = proinfoid
            AND s.type IN (2)
            AND sd.type = 2 AND sd.level_number = v_soMuc
            AND gd.type_apply = 2 AND gd.level_number = v_soMuc
        )t
      )
      SELECT t1.pro_gift_detail_id, t1.product_id, NVL(t1.soLuong, 0) soLuong, NVL(t1.donGia, 0) donGia FROM spKhaiBao t1
      UNION ALL
      SELECT t2.pro_gift_detail_id, t2.product_id, NVL(t2.soLuong, 0) soLuong, NVL(t2.donGia, 0) donGia FROM spChuaKhaiBao t2
      ;
  
    -- Loai 3: Tinh thuong chuong trinh HT03 Ho tro Ket/Thung,Tien cho Tong SL co TONG chung cho tat ca SP
    -- Xet dieu kien Dat hay Khong dat
    CURSOR c_TongChungDK (v_soMuc NUMBER)
    IS
      WITH
      -- MIN_LIMIT IS NOT NULL
      spDK AS(
        SELECT sd.product_id
        FROM pro_info i
        JOIN pro_structure s ON s.pro_info_id = i.pro_info_id
        JOIN pro_structure_detail sd ON sd.pro_structure_id = s.pro_structure_id
        WHERE 1=1
          AND i.pro_info_id = proinfoid AND i.type = 3 AND i.status = 1
          AND s.type IN (1)
          AND sd.type = 1 AND sd.min_limit IS NOT NULL AND sd.level_number = v_soMuc
      )
      ,spThoaDK AS(
        SELECT cp.product_id
        FROM pro_cus_process cp
        JOIN pro_cus_map cm ON cm.pro_cus_map_id = cp.pro_cus_map_id
        JOIN pro_cus_map_detail cmd ON cmd.pro_cus_map_id = cm.pro_cus_map_id AND cmd.product_id = cp.product_id
        JOIN pro_structure s ON s.pro_info_id = cm.pro_info_id
        JOIN pro_structure_detail sd ON sd.pro_structure_id = s.pro_structure_id AND sd.product_id = cp.product_id
        WHERE 1=1
          AND cp.pro_cus_map_id = procusmapid AND cp.pro_period_id = periodid
          AND s.type IN (1)
          AND cp.quantity >= (cmd.quantity*sd.min_limit/100)
          AND sd.type = 1 AND sd.min_limit IS NOT NULL AND sd.level_number = v_soMuc
          AND cm.level_number = v_soMuc
      )
      ,dongTong AS (
        SELECT (t1.slDangKy*t2.chanDuoi/100) chanDuoi
        FROM(
          SELECT SUM(cmd.quantity) slDangKy
          FROM pro_cus_map cm 
          JOIN pro_cus_map_detail cmd ON cmd.pro_cus_map_id = cm.pro_cus_map_id
          WHERE 1=1
            AND cm.pro_cus_map_id = procusmapid AND cm.level_number = v_soMuc
            )t1,
            (
          SELECT NVL(sd.min_limit, 0) chanDuoi
          FROM pro_structure s
          JOIN pro_structure_detail sd ON sd.pro_structure_id = s.pro_structure_id
          WHERE 1=1
            AND s.pro_info_id = proinfoid AND s.type IN (1)
            AND sd.type = 2 AND sd.level_number = v_soMuc
            )t2
      )
      -- Do trong bang PRO_CUS_PROCESS chi co 1 muc nen khong can xet muc o day
      ,tatCaSP AS(
        SELECT SUM(t.soLuong) soLuong
        FROM(
          SELECT cp.quantity soLuong
          FROM pro_cus_process cp
          WHERE 1=1
            AND cp.pro_cus_map_id = procusmapid AND cp.pro_period_id = periodid
            )t
      )
      SELECT CASE WHEN (SELECT 1 ketQua FROM dual WHERE (SELECT count(1) FROM spDK) = (SELECT count(1) FROM spThoaDK)
                        AND (SELECT t.soLuong FROM tatCaSP t) >= (SELECT t.chanDuoi FROM dongTong t)) IS NULL THEN 0
                  ELSE 1
            END ketQua
      FROM dual;
  
    -- Tinh thuong
    CURSOR c_TongChung (v_soMuc NUMBER)
    IS
      WITH
      dsSPChuaKBMax AS(
        SELECT sd.product_id
        FROM pro_info i
        JOIN pro_structure s ON s.pro_info_id = i.pro_info_id
        JOIN pro_structure_detail sd ON sd.pro_structure_id = s.pro_structure_id
        WHERE 1=1
          AND i.pro_info_id = proinfoid AND i.type = 3 AND i.status = 1
          AND s.type IN (1)
          AND sd.type = 1 AND sd.max_limit IS NULL AND sd.level_number = v_soMuc
      )
      -- Tinh thuong cho nhung san pham co khai bao (max_limit NOT NULL)
      ,spKhaiBao AS(
        SELECT gd.pro_gift_detail_id, cp.product_id
              ,CASE WHEN cp.quantity <= (cmd.quantity + (cmd.quantity*sd.max_limit/100)) THEN cp.quantity
              ELSE ROUND(cmd.quantity + (cmd.quantity*sd.max_limit/100)) END soLuong
              , NVL(gd.unit_cost, 0) donGia
        FROM pro_cus_process cp
        JOIN pro_cus_map cm ON cm.pro_cus_map_id = cp.pro_cus_map_id
        JOIN pro_cus_map_detail cmd ON cmd.pro_cus_map_id = cm.pro_cus_map_id AND cmd.product_id = cp.product_id
        JOIN pro_structure s ON s.pro_info_id = cm.pro_info_id
        JOIN pro_structure_detail sd ON sd.pro_structure_id = s.pro_structure_id AND sd.product_id = cp.product_id
        JOIN pro_gift g ON g.pro_structure_id = s.pro_structure_id
        JOIN pro_gift_detail gd ON gd.pro_gift_id = g.pro_gift_id AND gd.product_id = cp.product_id
        WHERE 1=1
          AND cp.pro_cus_map_id = procusmapid AND cp.pro_period_id = periodid
          AND sd.max_limit IS NOT NULL AND sd.type = 1 AND sd.level_number = v_soMuc
          AND cm.level_number = v_soMuc AND gd.level_number = v_soMuc
      )
      -- Tinh thuong cho nhung san pham chua khai bao (max_limit IS NULL)
      ,spChuaKhaiBao AS(
        SELECT t.pro_gift_detail_id, NULL product_id
              ,CASE WHEN t.slThucHien <= ((t.slDangKy + t.slDangKy*t.max_limit/100) - NVL((SELECT SUM(t.soLuong) FROM spKhaiBao t), 0)) THEN t.slThucHien
              ELSE (ROUND(t.slDangKy + t.slDangKy*t.max_limit/100) - NVL((SELECT SUM(t.soLuong) FROM spKhaiBao t), 0)) END soLuong
              ,t.donGia
        FROM(  
          -- Chi co 1 dong
          SELECT gd.pro_gift_detail_id, sd.min_limit, sd.max_limit
                ,NVL((SELECT SUM(cp.quantity) FROM pro_cus_process cp WHERE cp.pro_cus_map_id = procusmapid AND cp.pro_period_id = periodid AND cp.product_id IN (SELECT * FROM dsSPChuaKBMax) ), 0) slThucHien
                , (SELECT SUM(cmd.quantity) FROM pro_cus_map cm
                  JOIN pro_cus_map_detail cmd ON cm.pro_cus_map_id = cmd.pro_cus_map_id
                  WHERE cm.pro_cus_map_id = procusmapid AND cm.level_number = v_soMuc) slDangKy
                ,NVL(gd.unit_cost, 0) donGia
          FROM pro_info i
          JOIN pro_structure s ON s.pro_info_id = i.pro_info_id
          JOIN pro_structure_detail sd ON sd.pro_structure_id = s.pro_structure_id
          JOIN pro_gift g ON g.pro_structure_id = s.pro_structure_id
          JOIN pro_gift_detail gd ON gd.pro_gift_id = g.pro_gift_id
          WHERE 1=1
            AND i.pro_info_id = proinfoid
            AND s.type IN (1)
            AND sd.type = 2 AND sd.level_number = v_soMuc
            AND gd.type_apply = 2 AND gd.level_number = v_soMuc
        )t
      )
      SELECT * FROM spKhaiBao
      UNION ALL
      SELECT * FROM spChuaKhaiBao
      ;
  
    -- Loai ap dung: 1: khong co Tong, 2: Tong cho SP chua khai bao, 3: Tong cho tat ca SP
    CURSOR c_LoaiAD (v_soMuc NUMBER)
    IS
      SELECT CASE WHEN sd.min_limit IS NULL AND sd.max_limit IS NULL THEN 1
                  WHEN s.type = 2 THEN 2
              ELSE 3 END loaiAD
      FROM pro_info i
      JOIN pro_structure s ON s.pro_info_id = i.pro_info_id
      JOIN pro_structure_detail sd ON sd.pro_structure_id = s.pro_structure_id
      WHERE rownum = 1 AND i.pro_info_id = proinfoid AND sd.level_number = v_soMuc AND sd.type = 2;
    
    -- So muc do nguoi dung dang ky
    CURSOR c_SoMuc
    IS
      SELECT cm.level_number soMuc FROM pro_cus_map cm WHERE cm.pro_cus_map_id = procusmapid;

    loaiApDung NUMBER(3,0);
    rewardID NUMBER(20,0);
    result NUMBER(1,0);
    totalAmount NUMBER(20,0);
    soMuc NUMBER(20,0);
    
  BEGIN
    DBMS_OUTPUT.put_line ('PRO_HT03_CUS_REWARD started');
    OPEN c_SoMuc;
    FETCH c_SoMuc INTO soMuc;
    CLOSE c_SoMuc;

    OPEN c_LoaiAD (soMuc);
    FETCH c_LoaiAD INTO loaiApDung;
    CLOSE c_LoaiAD;
  
    rewardID := PRO_CUS_REWARD_SEQ.nextval;
    totalAmount := 0;

    -- Khong co tong
    IF (loaiApDung = 1) THEN
      DBMS_OUTPUT.put_line ('Loai khong co Tong');
      OPEN c_KTongDK (soMuc);
      FETCH c_KTongDK INTO result;
      IF c_KTongDK%notfound THEN result := 0;
      END IF;
      CLOSE c_KTongDK;

      IF (result = 1) THEN
        FOR v IN c_KTong (soMuc)
        LOOP
          totalAmount := totalAmount + v.donGia*v.soLuong;
          INSERT INTO pro_cus_reward_detail (PRO_CUS_REWARD_DETAIL_ID, PRO_CUS_REWARD_ID, PRO_GIFT_DETAIL_ID, QUANTITY_AUTO, UNIT_COST_AUTO, AMOUNT, CREATE_DATE, CREATE_user)
          VALUES (PRO_CUS_REWARD_DETAIL_SEQ.nextval, rewardID, v.pro_gift_detail_id, v.soLuong, v.donGia, v.soLuong*v.donGia, sysdate, username);
          DBMS_OUTPUT.put_line ('inserted 1 row into PRO_CUS_REWARD_DETAIL');
        END LOOP;
      END IF;
    ELSIF (loaiApDung = 2) THEN
      DBMS_OUTPUT.put_line ('Loai co Tong ap dung cho SP chua khai bao');
      OPEN c_TongChuaKBDK (soMuc);
      FETCH c_TongChuaKBDK INTO result;
      IF c_TongChuaKBDK%notfound THEN result := 0;
      END IF;
      CLOSE c_TongChuaKBDK;

      IF (result = 1) THEN
        FOR v IN c_TongChuaKB (soMuc)
        LOOP
          totalAmount := totalAmount + v.donGia*v.soLuong;
          INSERT INTO pro_cus_reward_detail (PRO_CUS_REWARD_DETAIL_ID, PRO_CUS_REWARD_ID, PRO_GIFT_DETAIL_ID, QUANTITY_AUTO, UNIT_COST_AUTO, AMOUNT, CREATE_DATE, CREATE_user)
          VALUES (PRO_CUS_REWARD_DETAIL_SEQ.nextval, rewardID, v.pro_gift_detail_id, v.soLuong, v.donGia, v.soLuong*v.donGia, sysdate, username);
          DBMS_OUTPUT.put_line ('inserted 1 row into PRO_CUS_REWARD_DETAIL');
        END LOOP;
      END IF;
    ELSE
      DBMS_OUTPUT.put_line ('Loai co Tong ap dung chung cho tat ca SP');
      OPEN c_TongChungDK (soMuc);
      FETCH c_TongChungDK INTO result;
      IF c_TongChungDK%notfound THEN result := 0;
      END IF;
      CLOSE c_TongChungDK;

      IF (result = 1) THEN
        FOR v IN c_TongChung (soMuc)
        LOOP
          totalAmount := totalAmount + v.donGia*v.soLuong;
          INSERT INTO pro_cus_reward_detail (PRO_CUS_REWARD_DETAIL_ID, PRO_CUS_REWARD_ID, PRO_GIFT_DETAIL_ID, QUANTITY_AUTO, UNIT_COST_AUTO, AMOUNT, CREATE_DATE, CREATE_user)
          VALUES (PRO_CUS_REWARD_DETAIL_SEQ.nextval, rewardID, v.pro_gift_detail_id, v.soLuong, v.donGia, v.soLuong*v.donGia, sysdate, username);
          DBMS_OUTPUT.put_line ('inserted 1 row into PRO_CUS_REWARD_DETAIL');
        END LOOP;
      END IF;
    END IF;
    
    INSERT INTO pro_cus_reward(PRO_CUS_REWARD_ID, PRO_CUS_MAP_ID, TOTAL_AMOUNT, CREATE_DATE, CREATE_user, PRO_PERIOD_ID, RESULT_AUTO, PRO_INFO_ID)
    VALUES(rewardID, procusmapid, totalAmount, sysdate, username, periodid, result, proinfoid);
    DBMS_OUTPUT.put_line ('inserted 1 row into PRO_CUS_REWARD');
    DBMS_OUTPUT.put_line ('Ket qua (0: khong dat, 1: dat): ' || result);
    COMMIT;
    DBMS_OUTPUT.put_line ('PRO_HT03_CUS_REWARD finished');
  END PRO_HT03_CUS_REWARD;