PROCEDURE PROCESS_FOR_CUSTOMER (proinfoid NUMBER, procusmapid NUMBER, objectId NUMBER, periodid NUMBER)
  -- Tinh thuc hien cho Customer luu vao bang PRO_CUS_PROCESS
  IS
    CURSOR c_Customer
    IS
      WITH
      tg AS(
        SELECT TRUNC(from_date) fromDate, TRUNC(to_date) toDate FROM pro_period WHERE pro_period_id = periodid
        )
      -- Chi lay nhung san pham nam trong Muc khach hang dang ky thuc hien
      -- Doi voi loai khong co Muc, thi LEVEL_NUMBER IS NULL trong bang PRO_CUS_MAP
      ,dsSP AS(
        SELECT sd.product_id
        FROM pro_cus_map cm
        JOIN pro_structure s ON s.pro_info_id = cm.pro_info_id
        JOIN pro_structure_detail sd ON sd.pro_structure_id = s.pro_structure_id
        WHERE 1=1 AND cm.pro_info_id = proinfoid AND cm.pro_cus_map_id = procusmapid AND s.type IN (1, 2)
          AND sd.product_id IS NOT NULL AND sd.type =1 AND (sd.level_number = cm.level_number OR cm.level_number IS NULL)
        union
        -- Ap dung HT02_N & HT06
        SELECT sd.product_id
        FROM pro_structure s
        JOIN pro_structure_detail sd ON sd.pro_structure_id = s.pro_structure_id
        WHERE 1=1 AND s.pro_info_id = proinfoid AND s.type IN (5, 6)
          AND sd.product_id IS NOT NULL AND sd.type IN (3)

      )
      ,xuatBan AS(
        SELECT sod.product_id, SUM(sod.quantity) giaTri
        FROM sale_order so
        JOIN sale_order_detail sod ON so.sale_order_id = sod.sale_order_id
        WHERE so.approved = 1 AND type IN (0,1)
            AND so.Approved_Date >= (SELECT t.fromDate FROM tg t) AND so.Approved_Date < (SELECT t.toDate FROM tg t) + 1
            AND so.customer_id = objectId AND sod.is_free_item = 0
        GROUP BY sod.product_id
      )
      ,nhapTraHang AS(
        SELECT sod.product_id, SUM(sod.quantity) giaTri
        FROM sale_order so
        JOIN sale_order_detail sod ON so.sale_order_id = sod.sale_order_id
        WHERE so.ORDER_TYPE = 'CM' AND so.approved = 1 AND type = 2
            AND so.CREATE_DATE >= (SELECT t.fromDate FROM tg t) AND so.CREATE_DATE < (SELECT t.toDate FROM tg t) + 1
            AND sod.CREATE_DATE >= (SELECT t.fromDate FROM tg t) AND sod.CREATE_DATE < (SELECT t.toDate FROM tg t) + 1
            AND so.customer_id = objectId AND sod.is_free_item = 0
        GROUP BY sod.product_id
      )
      SELECT sp.product_id
            ,(NVL((SELECT t.giaTri FROM xuatBan t WHERE t.product_id = sp.product_id), 0) 
                - NVL((SELECT t.giaTri FROM nhapTraHang t WHERE t.product_id = sp.product_id), 0)) muaTuNPP
            ,NVL((
                SELECT ROUND(SUM(psod.quantity/psod.convfact))
                FROM pg_sale_order pso
                JOIN pg_sale_order_detail psod ON psod.pg_sale_order_id = pso.pg_sale_order_id
                WHERE pso.order_date >= (SELECT t.fromDate FROM tg t) AND pso.order_date < (SELECT t.toDate FROM tg t) + 1
                    AND psod.order_date >= (SELECT t.fromDate FROM tg t) AND psod.order_date < (SELECT t.toDate FROM tg t) + 1
                    AND pso.customer_id = objectId AND psod.product_id = sp.product_id
            ),0) slTuPG
      FROM dsSP sp
      ;
    
  BEGIN
    DBMS_OUTPUT.put_line ('PROCESS_FOR_CUSTOMER started');
    DBMS_OUTPUT.put_line ('Customer ID = ' || objectId);
    FOR v IN c_Customer
    LOOP
        DELETE FROM pro_cus_process WHERE pro_cus_map_id = procusmapid AND product_id = v.product_id AND pro_info_id = proinfoid AND pro_period_id = periodid;
        
        INSERT INTO pro_cus_process(pro_cus_process_id,  pro_info_id, pro_cus_map_id, product_id,   quantity_shop,  quantity_pg,  quantity, create_date, create_user, object_id, object_type, pro_period_id)
                    VALUES (PRO_CUS_PROCESS_SEQ.nextval, proinfoid,   procusmapid,    v.product_id, v.muaTuNPP,     v.slTuPG,     v.slTuPG, sysdate,    'tien trinh', objectId  , 1,          periodid);
        DBMS_OUTPUT.put_line ('inserted 1 row into PRO_CUS_PROCESS');
    END LOOP;
    COMMIT;
    DBMS_OUTPUT.put_line ('PROCESS_FOR_CUSTOMER finished');
  END PROCESS_FOR_CUSTOMER;