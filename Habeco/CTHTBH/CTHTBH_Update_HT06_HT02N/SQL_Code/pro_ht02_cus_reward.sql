PROCEDURE PRO_HT02_CUS_REWARD (proinfoid NUMBER, procusmapid NUMBER, periodid NUMBER, username STRING)
  -- Tinh thuong HT02 luu vao bang PRO_CUS_REWARD va PRO_CUS_REWARD_DETAIL (neu co)
  IS
    -- Loai 1: Tinh thuong chuong trinh HT02 cho loai khong co dong Tong
    -- Xet dieu kien Dat hay Khong dat
    CURSOR c_KTongDK (v_sosuat IN NUMBER)
    IS
      WITH
      -- Lay danh sach SP co chan duoi SO LUONG (MIN_LIMIT IS NOT NULL)
      spDK AS(
        SELECT sd.product_id
        FROM pro_info i
        JOIN pro_structure s ON s.pro_info_id = i.pro_info_id
        JOIN pro_structure_detail sd ON sd.pro_structure_id = s.pro_structure_id
        WHERE 1=1
          AND i.pro_info_id = proinfoid AND i.type = 2 AND i.status = 1
          AND s.type IN (1, 2)
          AND sd.type = 1 AND sd.min_limit IS NOT NULL AND sd.level_number = v_sosuat
      )
      ,spThoaDK AS(
        SELECT cp.product_id
        FROM pro_cus_process cp
        JOIN pro_cus_map cm ON cm.pro_cus_map_id = cp.pro_cus_map_id
        JOIN pro_structure s ON s.pro_info_id = cm.pro_info_id
        JOIN pro_structure_detail sd ON sd.pro_structure_id = s.pro_structure_id AND sd.product_id = cp.product_id
        WHERE 1=1
          AND cp.pro_cus_map_id = procusmapid AND cp.pro_period_id = periodid
          AND s.type IN (1,2)
          AND (
              (cp.quantity >= sd.min_limit AND sd.unit_type = 1)
            OR 
              (cp.quantity*sd.convfact >= sd.min_limit AND sd.unit_type = 2)
              )
          AND sd.type = 1 AND sd.min_limit IS NOT NULL AND sd.level_number = v_sosuat
      )
      SELECT CASE WHEN (SELECT 1 ketQua FROM dual WHERE (SELECT count(1) FROM spDK) = (SELECT count(1) FROM spThoaDK)) IS NULL THEN 0
                  ELSE 1
            END ketQua
      FROM dual;
    
    -- Tinh he so
    CURSOR c_KTong (v_sosuat IN NUMBER)
    IS
      SELECT MIN(t.heSo) heSo
      FROM (
        SELECT s.pro_structure_id
              , sd.pro_structure_detail_id
              , CASE WHEN sd.unit_type = 1 THEN FLOOR(NVL(cp.quantity, 0) / sd.min_limit) 
                     WHEN sd.unit_type = 2 THEN FLOOR(NVL(cp.quantity, 0) * sd.convfact / sd.min_limit) 
                END heSo
              , sd.level_number soSuat
        FROM pro_cus_process cp
        JOIN pro_cus_map cm ON cm.pro_cus_map_id = cp.pro_cus_map_id
        JOIN pro_structure s ON s.pro_info_id = cm.pro_info_id
        JOIN pro_structure_detail sd ON sd.pro_structure_id = s.pro_structure_id AND sd.product_id = cp.product_id
        WHERE cp.pro_cus_map_id = procusmapid AND cp.pro_period_id = periodid AND sd.min_limit != 0 AND sd.level_number = v_sosuat
          )t;

    -- Loai 2: Tinh thuong chuong trinh HT02 co TONG tung SP chua khai bao
    -- Xet dieu kien Dat hay Khong dat
    CURSOR c_TongChuaKBDK (v_sosuat IN NUMBER)
    IS
      WITH
      tongSoSP AS(
        SELECT sd.product_id
        FROM pro_info i
        JOIN pro_structure s ON s.pro_info_id = i.pro_info_id
        JOIN pro_structure_detail sd ON sd.pro_structure_id = s.pro_structure_id
        WHERE 1=1
          AND i.pro_info_id = proinfoid AND i.type = 2 AND i.status = 1
          AND s.type IN (2)
          AND sd.type = 1 AND sd.level_number = v_sosuat
      )
      ,spDK AS(
        SELECT sd.product_id
        FROM pro_info i
        JOIN pro_structure s ON s.pro_info_id = i.pro_info_id
        JOIN pro_structure_detail sd ON sd.pro_structure_id = s.pro_structure_id
        WHERE 1=1
          AND i.pro_info_id = proinfoid AND i.type = 2 AND i.status = 1
          AND s.type IN (2)
          AND sd.type = 1 AND sd.min_limit IS NOT NULL AND sd.level_number = v_sosuat
      )
      ,spThoaDK AS(
        SELECT cp.product_id
        FROM pro_cus_process cp
        JOIN pro_cus_map cm ON cm.pro_cus_map_id = cp.pro_cus_map_id
        JOIN pro_structure s ON s.pro_info_id = cm.pro_info_id
        JOIN pro_structure_detail sd ON sd.pro_structure_id = s.pro_structure_id AND sd.product_id = cp.product_id
        WHERE 1=1
          AND cp.pro_cus_map_id = procusmapid AND cp.pro_period_id = periodid
          AND s.type IN (2)
          AND (
              (cp.quantity >= sd.min_limit AND sd.unit_type = 1)
            OR 
              (cp.quantity*sd.convfact >= sd.min_limit AND sd.unit_type = 2)
              )
          AND sd.type = 1 AND sd.min_limit IS NOT NULL AND sd.level_number = v_sosuat
      )
      ,dongTong AS (
        SELECT sd.min_limit chanDuoi
        FROM pro_structure s
        JOIN pro_structure_detail sd ON sd.pro_structure_id = s.pro_structure_id
        WHERE 1=1
          AND s.pro_info_id = proinfoid
          AND s.type IN (2)
          AND sd.type = 2 AND sd.level_number = v_sosuat
      )
      ,spKhongDK AS(
        SELECT SUM(t.soLuong) soLuong
        FROM(
          SELECT CASE WHEN sd.unit_type = 1 THEN cp.quantity
                      WHEN sd.unit_type = 2 THEN cp.quantity * sd.convfact
                 END soLuong
          FROM pro_cus_process cp
          JOIN pro_cus_map cm ON cm.pro_cus_map_id = cp.pro_cus_map_id
          JOIN pro_structure s ON s.pro_info_id = cm.pro_info_id
          JOIN pro_structure_detail sd ON sd.pro_structure_id = s.pro_structure_id AND sd.product_id = cp.product_id
          WHERE 1=1
            AND cp.pro_cus_map_id = procusmapid AND cp.pro_period_id = periodid
            AND cp.product_id NOT IN (SELECT t.product_id FROM spDK t)
            AND s.type IN (2)
            AND sd.type = 1 AND sd.level_number = v_sosuat
            )t
      )
      -- Ket qua Dat, neu cac SP dieu kien thoa dieu kien, cac SP chua khai bao thoa dieu kien Tong hoac khong co SP chua khai bao
      SELECT CASE WHEN (SELECT 1 ketQua FROM dual WHERE 
                          ((SELECT count(1) FROM spDK) = (SELECT count(1) FROM spThoaDK) AND (SELECT t.soLuong FROM spKhongDK t) >= (SELECT t.chanDuoi FROM dongTong t))
                          OR
                          ((SELECT count(1) FROM spDK) = (SELECT count(1) FROM spThoaDK) AND (SELECT count(1) FROM spDK) = (SELECT count(1) FROM tongSoSP))
                        ) IS NULL THEN 0
                  ELSE 1
            END ketQua
      FROM dual;
    
    -- Tinh he so
    CURSOR c_TongChuaKB (v_sosuat IN NUMBER)
    IS
      WITH
      -- MIN_LIMIT IS NULL
      spChuaKB AS(
        SELECT sd.product_id
        FROM pro_info i
        JOIN pro_structure s ON s.pro_info_id = i.pro_info_id
        JOIN pro_structure_detail sd ON sd.pro_structure_id = s.pro_structure_id
        WHERE 1=1
          AND i.pro_info_id = proinfoid AND i.type = 2 AND i.status = 1
          AND s.type IN (2)
          AND sd.type = 1 AND sd.min_limit IS NULL AND sd.level_number = v_sosuat
      )
      -- MIN_LIMIT IS NOT NULL
      ,spKhaiBao AS(
        SELECT sd.product_id
        FROM pro_info i
        JOIN pro_structure s ON s.pro_info_id = i.pro_info_id
        JOIN pro_structure_detail sd ON sd.pro_structure_id = s.pro_structure_id
        WHERE 1=1
          AND i.pro_info_id = proinfoid AND i.type = 2 AND i.status = 1
          AND s.type IN (2)
          AND sd.type = 1 AND sd.min_limit IS NOT NULL AND sd.level_number = v_sosuat
      )
      ,heSoSPKB AS(
        SELECT MIN(t.heSo) heSo
        FROM (
          SELECT sd.pro_structure_detail_id
                , CASE WHEN sd.unit_type = 1 THEN FLOOR(NVL(cp.quantity, 0) / sd.min_limit) 
                       WHEN sd.unit_type = 2 THEN FLOOR(NVL(cp.quantity, 0) * sd.convfact / sd.min_limit) 
                  END heSo
          FROM pro_cus_process cp
          JOIN pro_cus_map cm ON cm.pro_cus_map_id = cp.pro_cus_map_id
          JOIN pro_structure s ON s.pro_info_id = cm.pro_info_id
          JOIN pro_structure_detail sd ON sd.pro_structure_id = s.pro_structure_id AND sd.product_id = cp.product_id
          WHERE cp.pro_cus_map_id = procusmapid AND cp.pro_period_id = periodid AND sd.min_limit != 0 AND sd.level_number = v_sosuat
            AND cp.product_id IN (SELECT t.product_id FROM spKhaiBao t)
            )t
      )
      ,heSoSPChuaKB AS(
        SELECT FLOOR(NVL(t.heSo, 0) / t.min_limit) heSo
        FROM(  
          -- Chi co 1 dong
          SELECT sd.min_limit
                , CASE WHEN sd.unit_type = 1 THEN
                        NVL((SELECT SUM(cp.quantity) FROM pro_cus_process cp WHERE cp.pro_cus_map_id = procusmapid AND cp.pro_period_id = periodid
                          AND cp.product_id IN (SELECT t.product_id FROM spChuaKB t)), 0)
                        WHEN sd.unit_type = 2 THEN
                        NVL((SELECT SUM(cp.quantity * sd.convfact) FROM pro_cus_process cp WHERE cp.pro_cus_map_id = procusmapid AND cp.pro_period_id = periodid
                          AND cp.product_id IN (SELECT t.product_id FROM spChuaKB t)), 0)
                  END heSo
          FROM pro_cus_map cm
          JOIN pro_info i ON i.pro_info_id = cm.pro_info_id
          JOIN pro_structure s ON s.pro_info_id = i.pro_info_id
          JOIN pro_structure_detail sd ON sd.pro_structure_id = s.pro_structure_id
          WHERE 1=1
            AND cm.pro_cus_map_id = procusmapid AND s.type IN (2) AND sd.type = 2 AND i.type = 2 AND i.status = 1 AND sd.min_limit != 0  AND sd.level_number = v_sosuat
        )t
      )
      SELECT CASE WHEN (SELECT product_id FROM spChuaKB  WHERE ROWNUM = 1) IS NULL THEN (SELECT heSo FROM heSoSPKB)
                WHEN (SELECT product_id FROM spKhaiBao WHERE ROWNUM = 1) IS NULL THEN (SELECT heSo FROM heSoSPChuaKB)
                ELSE LEAST((SELECT heSo FROM heSoSPKB), (SELECT heSo FROM heSoSPChuaKB))
            END heSo
      FROM dual;

    -- Loai 3: Tinh thuong chuong trinh HT02 co TONG chung cho tat ca SP
    -- Xet dieu kien Dat hay Khong dat
    CURSOR c_TongChungDK (v_sosuat IN NUMBER)
    IS
      WITH
      spDK AS(
        SELECT sd.product_id
        FROM pro_info i
        JOIN pro_structure s ON s.pro_info_id = i.pro_info_id
        JOIN pro_structure_detail sd ON sd.pro_structure_id = s.pro_structure_id
        WHERE 1=1
          AND i.pro_info_id = proinfoid AND i.type = 2 AND i.status = 1
          AND s.type IN (1)
          AND sd.type = 1 AND sd.min_limit IS NOT NULL AND sd.level_number = v_sosuat
      )
      ,spThoaDK AS(
        SELECT cp.product_id
        FROM pro_cus_process cp
        JOIN pro_cus_map cm ON cm.pro_cus_map_id = cp.pro_cus_map_id
        JOIN pro_structure s ON s.pro_info_id = cm.pro_info_id
        JOIN pro_structure_detail sd ON sd.pro_structure_id = s.pro_structure_id AND sd.product_id = cp.product_id
        WHERE 1=1
          AND cp.pro_cus_map_id = procusmapid AND cp.pro_period_id = periodid
          AND s.type IN (1)
          AND (
              (cp.quantity >= sd.min_limit AND sd.unit_type = 1)
            OR 
              (cp.quantity*sd.convfact >= sd.min_limit AND sd.unit_type = 2)
              )
          AND sd.type = 1 AND sd.min_limit IS NOT NULL AND sd.level_number = v_sosuat
      )
      ,dongTong AS (
        SELECT sd.min_limit chanDuoi
        FROM pro_structure s
        JOIN pro_structure_detail sd ON sd.pro_structure_id = s.pro_structure_id
        WHERE 1=1
          AND s.pro_info_id = proinfoid
          AND s.type IN (1)
          AND sd.type = 2 AND sd.level_number = v_sosuat
      )
      ,tatCaSP AS(
        SELECT SUM(t.soLuong) soLuong
        FROM(
          SELECT CASE WHEN sd.unit_type = 1 THEN cp.quantity
                      WHEN sd.unit_type = 2 THEN cp.quantity * sd.convfact
                 END soLuong
          FROM pro_cus_process cp
          JOIN pro_cus_map cm ON cm.pro_cus_map_id = cp.pro_cus_map_id
          JOIN pro_structure s ON s.pro_info_id = cm.pro_info_id
          JOIN pro_structure_detail sd ON sd.pro_structure_id = s.pro_structure_id AND sd.product_id = cp.product_id
          WHERE 1=1
            AND cp.pro_cus_map_id = procusmapid AND cp.pro_period_id = periodid
            AND s.type IN (1)
            AND sd.type = 1 AND sd.level_number = v_sosuat
            )t
      )
      SELECT CASE WHEN (SELECT 1 ketQua FROM dual WHERE (SELECT count(1) FROM spDK) = (SELECT count(1) FROM spThoaDK)
                        AND (SELECT t.soLuong FROM tatCaSP t) >= (SELECT t.chanDuoi FROM dongTong t)) IS NULL THEN 0
                  ELSE 1
            END ketQua
      FROM dual;
    
    -- Tinh he so
    CURSOR c_TongChung (v_sosuat IN NUMBER)
    IS
      WITH
      tongSoSP AS(
        SELECT sd.product_id
        FROM pro_info i
        JOIN pro_structure s ON s.pro_info_id = i.pro_info_id
        JOIN pro_structure_detail sd ON sd.pro_structure_id = s.pro_structure_id
        WHERE 1=1
          AND i.pro_info_id = proinfoid AND i.type = 2 AND i.status = 1
          AND s.type IN (1)
          AND sd.type = 1 AND sd.level_number = v_sosuat
      )
      ,spKhaiBao AS(
        SELECT sd.product_id
        FROM pro_info i
        JOIN pro_structure s ON s.pro_info_id = i.pro_info_id
        JOIN pro_structure_detail sd ON sd.pro_structure_id = s.pro_structure_id
        WHERE 1=1
          AND i.pro_info_id = proinfoid AND i.type = 2 AND i.status = 1
          AND s.type IN (1)
          AND sd.type = 1 AND sd.min_limit IS NOT NULL AND sd.level_number = v_sosuat
      )
      ,heSoSPKB AS(
        SELECT MIN(t.heSo) heSo
        FROM (
          SELECT sd.pro_structure_detail_id
                , CASE WHEN sd.unit_type = 1 THEN FLOOR(NVL(cp.quantity, 0) / sd.min_limit) 
                       WHEN sd.unit_type = 2 THEN FLOOR(NVL(cp.quantity, 0) * sd.convfact / sd.min_limit) 
                  END heSo
          FROM pro_cus_process cp
          JOIN pro_cus_map cm ON cm.pro_cus_map_id = cp.pro_cus_map_id
          JOIN pro_info i ON i.pro_info_id = cm.pro_info_id
          JOIN pro_structure s ON s.pro_info_id = i.pro_info_id
          JOIN pro_structure_detail sd ON sd.pro_structure_id = s.pro_structure_id AND sd.product_id = cp.product_id
          WHERE cp.pro_cus_map_id = procusmapid AND cp.pro_period_id = periodid AND sd.min_limit != 0 AND sd.level_number = v_sosuat
            AND cp.product_id IN (SELECT t.product_id FROM spKhaiBao t)
            )t
      )
      ,heSoSPChuaKB AS(
        SELECT FLOOR(NVL(t.heSo, 0) / t.min_limit) heSo
        FROM(  
          -- Chi co 1 dong
          SELECT sd.min_limit
                , CASE WHEN sd.unit_type = 1 THEN
                        NVL((SELECT SUM(cp.quantity) FROM pro_cus_process cp WHERE cp.pro_cus_map_id = procusmapid AND cp.pro_period_id = periodid
                          AND cp.product_id IN (SELECT t.product_id FROM tongSoSP t)), 0)
                        WHEN sd.unit_type = 2 THEN
                        NVL((SELECT SUM(cp.quantity * sd.convfact) FROM pro_cus_process cp WHERE cp.pro_cus_map_id = procusmapid AND cp.pro_period_id = periodid
                          AND cp.product_id IN (SELECT t.product_id FROM tongSoSP t)), 0)
                  END heSo
          FROM pro_cus_map cm
          JOIN pro_info i ON i.pro_info_id = cm.pro_info_id
          JOIN pro_structure s ON s.pro_info_id = i.pro_info_id
          JOIN pro_structure_detail sd ON sd.pro_structure_id = s.pro_structure_id
          WHERE 1=1
            AND cm.pro_cus_map_id = procusmapid AND s.type IN (1) AND sd.type = 2 AND i.type = 2 AND i.status = 1 AND sd.min_limit != 0 AND sd.level_number = v_sosuat
        )t
      )
      SELECT
        CASE 
            WHEN (SELECT product_id FROM spKhaiBao WHERE ROWNUM = 1) IS NULL THEN (SELECT heSo FROM heSoSPChuaKB)
            ELSE LEAST((SELECT heSo FROM heSoSPKB), (SELECT heSo FROM heSoSPChuaKB))
        END heSo
      FROM dual;

    -- Thong tin ho tro theo suat
    CURSOR c_HoTro (v_sosuat IN NUMBER)
    IS
      SELECT gd.pro_gift_detail_id, gd.unit_cost donGia, gd.quantity soLuong, pi.object_type loaiHoTro
      FROM pro_structure s
      JOIN pro_gift g ON g.pro_structure_id = s.pro_structure_id
      JOIN pro_gift_detail gd ON gd.pro_gift_id = g.pro_gift_id
      JOIN product_info pi ON pi.product_info_id = gd.type_support
      WHERE s.pro_info_id = proinfoid AND gd.level_number = v_sosuat AND pi.type = 1
      ;
    -- Lay ra tong so suat
    CURSOR c_Suat
    IS
      SELECT t.soSuat
      FROM (
        SELECT DISTINCT sd.level_number soSuat
        FROM pro_structure s
        JOIN pro_structure_detail sd ON sd.pro_structure_id = s.pro_structure_id
        WHERE s.pro_info_id = proinfoid
        )t
      ORDER BY t.soSuat
      ;

    -- Lay ra han muc, han muc co the = NULL hoac > 0 (khong co han muc = 0)
    CURSOR c_HanMuc
    IS
      SELECT s.quota giaTri FROM pro_structure s WHERE s.pro_info_id = proinfoid;

    -- Loai ap dung: 1: khong co Tong, 2: Tong cho SP chua khai bao, 3: Tong cho tat ca SP
    CURSOR c_LoaiAD (v_sosuat IN NUMBER)
    IS
      SELECT CASE WHEN sd.min_limit IS NULL THEN 1
                  WHEN s.type = 2 THEN 2
                  ELSE 3 END loaiAD
      FROM pro_info i
      JOIN pro_structure s ON s.pro_info_id = i.pro_info_id
      JOIN pro_structure_detail sd ON sd.pro_structure_id = s.pro_structure_id
      WHERE rownum = 1 AND i.pro_info_id = proinfoid AND sd.level_number = v_sosuat AND sd.type = 2;
  
    -- Danh cho loai cap nhat bang tay: Lay PRO_CUS_REWARD_ID
    CURSOR c_ProCusReward
    IS
      SELECT pro_cus_reward_id
      FROM pro_cus_reward
      WHERE pro_cus_map_id = procusmapid AND pro_period_id = periodid;
    
    loaiApDung NUMBER(3,0);
    rewardID NUMBER(20,0);
    rewardDetailID NUMBER(20,0);
    soLuong NUMBER(20,0);
    donGia NUMBER(20,0);
    result NUMBER(1,0);
    totalAmount NUMBER(20,0);
    isAchieved NUMBER(20, 0) := 0;
    hanMuc NUMBER(20, 0);
    heSoTmp NUMBER(20,0);
    heSo NUMBER(20,0);
    soSuatThuong NUMBER(20,0) := 0;
    
  BEGIN
    DBMS_OUTPUT.put_line ('PRO_HT02_CUS_REWARD started');
    OPEN c_HanMuc;
    FETCH c_HanMuc INTO hanMuc;
    CLOSE c_HanMuc;

    totalAmount := 0;
    DBMS_OUTPUT.put_line ('Chay theo tien trinh');
    rewardID := PRO_CUS_REWARD_SEQ.nextval;
    
    FOR s IN c_Suat
    LOOP
      -- Neu con han muc
      IF (hanMuc > 0 OR hanMuc IS NULL) THEN
        OPEN c_LoaiAD (s.soSuat);
        FETCH c_LoaiAD INTO loaiApDung;
        CLOSE c_LoaiAD;

        -- Khong co tong
        IF (loaiApDung = 1) THEN
          DBMS_OUTPUT.put_line ('Suat ' || s.soSuat || ': Loai khong co Tong');
          OPEN c_KTongDK (s.soSuat);
          FETCH c_KTongDK INTO result;
          IF c_KTongDK%notfound THEN result := 0;
          END IF;
          CLOSE c_KTongDK;

          OPEN c_KTong(s.soSuat);
          FETCH c_KTong INTO heSoTmp;
          CLOSE c_KTong;

        -- Tong ap dung cho SP chua khai bao
        ELSIF (loaiApDung = 2) THEN
          DBMS_OUTPUT.put_line ('Suat ' || s.soSuat || ': Loai co Tong ap dung cho SP chua khai bao');
          OPEN c_TongChuaKBDK (s.soSuat);
          FETCH c_TongChuaKBDK INTO result;
          IF c_TongChuaKBDK%notfound THEN result := 0;
          END IF;
          CLOSE c_TongChuaKBDK;

          OPEN c_TongChuaKB(s.soSuat);
          FETCH c_TongChuaKB INTO heSoTmp;
          CLOSE c_TongChuaKB;

        -- Tong ap dung chung cho tat ca SP
        ELSE
          DBMS_OUTPUT.put_line ('Suat ' || s.soSuat || ': Loai co Tong ap dung chung cho tat ca SP');
          OPEN c_TongChungDK (s.soSuat);
          FETCH c_TongChungDK INTO result;
          IF c_TongChungDK%notfound THEN result := 0;
          END IF;
          CLOSE c_TongChungDK;

          OPEN c_TongChung(s.soSuat);
          FETCH c_TongChung INTO heSoTmp;
          CLOSE c_TongChung;
        END IF;

        -- Suat dat
        IF (result = 1) THEN
          DBMS_OUTPUT.put_line ('Suat ' || s.soSuat || ': Dat thuong');
          isAchieved := 1;
          
          IF (hanMuc IS NULL) THEN
            heSo := heSoTmp;
          ELSIF (hanMuc < heSoTmp AND hanMuc IS NOT NULL) THEN
            heSo := hanMuc;
            hanMuc := 0;
          ELSIF (hanMuc >= heSoTmp AND hanMuc IS NOT NULL) THEN
            hanMuc := hanMuc - heSoTmp;
            heSo := heSoTmp;
          END IF;

          soSuatThuong := soSuatThuong + heSo;

          FOR v IN c_HoTro (s.soSuat)
          LOOP
            totalAmount := totalAmount + v.soLuong*heSo*v.donGia;
            IF (v.loaiHoTro = 4) THEN
              INSERT INTO pro_cus_reward_detail (PRO_CUS_REWARD_DETAIL_ID, PRO_CUS_REWARD_ID, PRO_GIFT_DETAIL_ID, QUANTITY_AUTO, UNIT_COST_AUTO, AMOUNT, CREATE_DATE, CREATE_USER)
              VALUES (PRO_CUS_REWARD_DETAIL_SEQ.nextval, rewardID, v.pro_gift_detail_id, v.soLuong, v.donGia*heSo, v.soLuong*heSo*v.donGia, sysdate, username);
            ELSE
              INSERT INTO pro_cus_reward_detail (PRO_CUS_REWARD_DETAIL_ID, PRO_CUS_REWARD_ID, PRO_GIFT_DETAIL_ID, QUANTITY_AUTO, UNIT_COST_AUTO, AMOUNT, CREATE_DATE, CREATE_USER)
              VALUES (PRO_CUS_REWARD_DETAIL_SEQ.nextval, rewardID, v.pro_gift_detail_id, v.soLuong*heSo, v.donGia, v.soLuong*heSo*v.donGia, sysdate, username);
            END IF;
            DBMS_OUTPUT.put_line ('inserted 1 row into PRO_CUS_REWARD_DETAIL');
          END LOOP;
        ELSE
          DBMS_OUTPUT.put_line ('Suat ' || s.soSuat || ': Khong dat thuong');
        END IF;
      END IF;
    END LOOP;
    INSERT INTO pro_cus_reward(PRO_CUS_REWARD_ID, PRO_CUS_MAP_ID, TOTAL_AMOUNT, LEVEL_AUTO, CREATE_DATE, CREATE_USER, PRO_PERIOD_ID, RESULT_AUTO, PRO_INFO_ID)
    VALUES(rewardID, procusmapid, totalAmount, soSuatThuong, sysdate, username, periodid, isAchieved, proinfoid);
    DBMS_OUTPUT.put_line ('inserted 1 row into PRO_CUS_REWARD');
    DBMS_OUTPUT.put_line ('Ket qua ' || isAchieved);
    COMMIT;
    DBMS_OUTPUT.put_line ('PRO_HT02_CUS_REWARD finished');
  END PRO_HT02_CUS_REWARD;