PROCEDURE PRO_CUS_PROCESS (inputDate DATE)
  -- Duyet qua bang PRO_PERIOD neu TO_DATE < inputDate va PRO_PERIOD_ID chua co du lieu trong bang PRO_CUS_PROCESS thi thuc hien
  IS
    CURSOR c_ProPeriod
    IS
      WITH
      proPeriod AS(
        SELECT pp.pro_period_id, pp.pro_info_id, pp.from_date, pp.to_date
        FROM pro_period pp
        WHERE 1=1
          AND pp.to_date < inputDate AND pp.pro_period_id NOT IN (SELECT t.pro_period_id FROM pro_cus_process t)
          )
      ,proCusMap AS(
        SELECT cm.pro_cus_map_id, cm.pro_info_id, ch.from_date, ch.to_date
        FROM pro_cus_map cm
        JOIN pro_cus_history ch ON ch.pro_cus_map_id = cm.pro_cus_map_id
        WHERE ch.type = 1
        )
      SELECT pp.pro_info_id, pp.pro_period_id, cm.pro_cus_map_id, pp.from_date, pp.to_date
      FROM proPeriod pp, proCusMap cm
      WHERE pp.pro_info_id = cm.pro_info_id AND TRUNC(cm.from_date) <= TRUNC(pp.from_date)
      AND (cm.to_date IS NULL OR (TRUNC(cm.to_date) >= TRUNC(pp.to_date)))
      ;

    -- Loai doi tuong: 1: customer, 2: npp
    CURSOR c_LoaiDT(v_CusMapId NUMBER)
    IS
      SELECT object_type, object_id FROM pro_cus_map WHERE pro_cus_map_id = v_CusMapId;
  
    loaiDoiTuong NUMBER(3,0);
    idDoiTuong NUMBER(20,0);
    x_param NUMBER(3,0);
    y_param NUMBER(3,0);
    x_Date Date;
    y_Date DATE;
  BEGIN
    DBMS_OUTPUT.put_line ('PRO_CUS_PROCESS started');
    SELECT value INTO x_param FROM ap_param WHERE ap_param_code = 'START_DAY_REWARD';
    SELECT value INTO y_param FROM ap_param WHERE ap_param_code = 'LAST_DAY_REWARD';

    FOR v IN c_ProPeriod
    LOOP
      x_Date := v.to_date + x_param;
      y_Date := v.to_date + y_param;
      OPEN c_LoaiDT(v.pro_cus_map_id);
      FETCH c_LoaiDT INTO loaiDoiTuong, idDoiTuong;
      CLOSE c_LoaiDT;
      -- Loai doi tuong Customer
      IF (loaiDoiTuong = 1 AND inputDate >= TRUNC(x_Date)) THEN
        PROCESS_FOR_CUSTOMER (v.pro_info_id, v.pro_cus_map_id, idDoiTuong, v.pro_period_id);
      -- Loai doi tuong Shop
      ELSIF (loaiDoiTuong = 2 AND inputDate >= TRUNC(y_Date) + 1) THEN
        PROCESS_FOR_SHOP (v.pro_info_id, v.pro_cus_map_id, idDoiTuong, v.pro_period_id);
      END IF;
    END LOOP;
    DBMS_OUTPUT.put_line ('PRO_CUS_PROCESS finished');
  END PRO_CUS_PROCESS;