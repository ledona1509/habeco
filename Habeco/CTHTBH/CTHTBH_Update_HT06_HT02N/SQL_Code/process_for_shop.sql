PROCEDURE PROCESS_FOR_SHOP (proinfoid NUMBER, procusmapid NUMBER, objectId NUMBER, periodid NUMBER)
  -- Tinh thuc hien cho SHOP luu vao bang PRO_CUS_PROCESS
  IS
    CURSOR c_NPP
    IS
      WITH
      tg AS(
        SELECT TRUNC(from_date) fromDate, TRUNC(to_date) toDate FROM pro_period WHERE pro_period_id = periodid
        )
      -- Chi lay nhung san pham nam trong Muc khach hang dang ky thuc hien
      -- Doi voi loai khong co Muc, thi LEVEL_NUMBER IS NULL trong bang PRO_CUS_MAP
      ,dsSP AS(
        SELECT sd.product_id
        FROM pro_cus_map cm
        JOIN pro_structure s ON s.pro_info_id = cm.pro_info_id
        JOIN pro_structure_detail sd ON sd.pro_structure_id = s.pro_structure_id
        WHERE 1=1 AND cm.pro_info_id = proinfoid AND cm.pro_cus_map_id = procusmapid AND s.type IN (1, 2)
          AND sd.product_id IS NOT NULL AND sd.type =1 AND (sd.level_number = cm.level_number OR cm.level_number IS NULL)
        union
        -- Ap dung HT02_N & HT06
        SELECT sd.product_id
        FROM pro_structure s
        JOIN pro_structure_detail sd ON sd.pro_structure_id = s.pro_structure_id
        WHERE 1=1 AND s.pro_info_id = proinfoid AND s.type IN (5, 6)
          AND sd.product_id IS NOT NULL AND sd.type IN (3)
      )
      ,thucHien AS(
        SELECT rpt.product_id, SUM(rpt.xuat_ban - rpt.nhap_tra_hang) xuatBan, SUM(rpt.nhap_sbc) nhapTra
        FROM rpt_stock_total_day rpt
        WHERE 1=1
          AND rpt.shop_id IN (SELECT object_id FROM pro_cus_map WHERE pro_cus_map_id = procusmapid AND object_type = 2)
          AND rpt.rpt_in_date >= (SELECT t.fromDate FROM tg t) AND rpt.rpt_in_date < (SELECT t.toDate FROM tg t) + 1
        GROUP BY rpt.product_id
      )
      SELECT sp.product_id
            ,NVL((SELECT t.xuatBan FROM thucHien t WHERE t.product_id = sp.product_id), 0) xuatBan
            ,NVL((SELECT t.nhapTra FROM thucHien t WHERE t.product_id = sp.product_id), 0) nhapTra
      FROM dsSP sp
      ;
    
  BEGIN
    DBMS_OUTPUT.put_line ('PROCESS_FOR_SHOP started');
    DBMS_OUTPUT.put_line ('Shop ID = ' || objectId);
    FOR v IN c_NPP
    LOOP
      DELETE FROM pro_cus_process WHERE pro_cus_map_id = procusmapid AND product_id = v.product_id AND pro_info_id = proinfoid AND pro_period_id = periodid;
      
      -- 18/03 Bo sung them insert nhap tra hang HT02_N, HT06
      INSERT INTO pro_cus_process(pro_cus_process_id, pro_info_id, pro_cus_map_id, product_id,  quantity, quantity_return, CREATE_DATE,CREATE_USER, OBJECT_ID, OBJECT_TYPE, PRO_PERIOD_ID)
                 VALUES (PRO_CUS_PROCESS_SEQ.nextval, proinfoid  , procusmapid  , v.product_id, v.xuatBan, v.nhapTra     , sysdate   , 'tien trinh', objectId, 2          , periodid);
      DBMS_OUTPUT.put_line ('inserted 1 row into PRO_CUS_PROCESS');
    END LOOP;
    COMMIT;
    DBMS_OUTPUT.put_line ('PROCESS_FOR_SHOP finished');
  END PROCESS_FOR_SHOP;