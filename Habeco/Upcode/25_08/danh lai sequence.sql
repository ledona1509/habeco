SET serveroutput on; 
DECLARE
  maxID number;
  isExistSequence number;
  qGetMaxTableID varchar(200);
  qExistSequence varchar(200);
  qDropSequence varchar(200);
  qCreateSequence varchar(200);
  tableName varchar(100);

  -- lay ra ds ten tat ca cac table
  CURSOR c1
  IS
  SELECT OBJECT_NAME FROM user_objects WHERE object_type = 'TABLE' ;
  begin
  OPEN c1;
    LOOP
    begin
      -- lay ra ten tung bang
      fetch c1 into tableName;
      EXIT WHEN c1%NOTFOUND;
      dbms_output.put_line('Ten table: ' || tableName);
      -- lay ra max ID bang
      qGetMaxTableID := 'SELECT nvl(max(' || tableName || '_ID),0) FROM ' || tableName || ' WHERE ' || tableName || '_ID < 100000000';
      
      EXECUTE IMMEDIATE qGetMaxTableID into maxID;
      dbms_output.put_line('Max ' || tableName || '_ID: ' || maxID);
      -- IF maxID = 0 THEN
        -- qGetMaxTableID := 'SELECT nvl(max(ID),0) FROM ' || tableName || ' WHERE ID < 100000000';
       
        -- EXECUTE IMMEDIATE qGetMaxTableID into maxID;
      -- END IF;
      
      -- lay ra so luong sequence cua bang TABLENAME_SEQUENCE, thuong la 1
      qExistSequence := 'SELECT count(*) FROM user_sequences WHERE sequence_name = ''' || tableName || '_SEQ''';
       
      EXECUTE IMMEDIATE qExistSequence into isExistSequence;
      
      -- neu da ton tai sequence thi drop bo di
      if isExistSequence > 0  then
        dbms_output.put_line('Thuc hien xoa sequence ' || tableName || '_SEQ');
        qDropSequence := 'DROP SEQUENCE ' || tableName || '_SEQ';
        EXECUTE IMMEDIATE qDropSequence;
      end if;
      
      -- tao lai sequence, voi vi tri bat dau = max ID + 1
      dbms_output.put_line('Tao sequence ' || tableName || '_SEQ co chi so bat dau = ' || (maxID+1));
      qCreateSequence := 'CREATE SEQUENCE  ' || tableName ||'_SEQ MINVALUE 1 MAXVALUE 999999999999999999999999999 INCREMENT BY 1 START WITH ' || (maxID+1) || ' NOCACHE  NOORDER  NOCYCLE';
      EXECUTE IMMEDIATE qCreateSequence;
      
      exception
      WHEN OTHERS THEN NULL; 
      end;
      dbms_output.put_line('-----------------------');
     END LOOP;
     CLOSE c1;
     dbms_output.put_line('Xong');
  end;