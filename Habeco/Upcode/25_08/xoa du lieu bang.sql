﻿SET serveroutput on; 
DECLARE
  gDeleteTable varchar(200);
  tableName varchar(100);

  -- lay ra ds ten tat ca cac table
  CURSOR c1
  IS
  SELECT OBJECT_NAME FROM user_objects WHERE object_type = 'TABLE' AND OBJECT_NAME NOT IN ('AP_PARAM','AREA','PRODUCT_INFO','REPORT','REPORT_GROUP','REPORT_GROUP_DETAIL','DATABASE_VERSION','APPLICATION_VERSION');
  begin
  OPEN c1;
    LOOP
    begin
      -- lay ra ten tung bang
      fetch c1 into tableName;
      EXIT WHEN c1%NOTFOUND;
      -- xoa bang
      gDeleteTable := 'DELETE ' || tableName;
      dbms_output.put_line('DELETED ' || tableName);
      EXECUTE IMMEDIATE gDeleteTable;
      COMMIT;
      exception
      WHEN OTHERS THEN NULL; 
      end;
     END LOOP;
     CLOSE c1;
     dbms_output.put_line('Xong');
  end;