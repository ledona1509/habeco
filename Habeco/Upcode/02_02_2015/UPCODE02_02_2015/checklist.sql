---I. Backup
----1. Backup body cac procedure CRM o file 'BackUpCRMBody.sql'
----2. Backup procedure P_RPT_STAFF_FAULT o file 'BackUp_P_RPT_STAFF_FAULT.sql'

---II. Thay doi
----1. Module lich huan luyen
-----a. Them bang EVENT
CREATE TABLE "HABECO_RELEASE"."EVENT" 
   (	"EVENT_ID" NUMBER(20,0), 
	"EVENT_NAME" VARCHAR2(100 BYTE) NOT NULL ENABLE, 
	"EVENT_DESCRIPTION" VARCHAR2(200 BYTE), 
	"EVENT_TYPE" VARCHAR2(50 BYTE) NOT NULL ENABLE, 
	"ADDRESS" VARCHAR2(200 BYTE), 
	"AREA_ID" NUMBER(20,0), 
	"TRAINING_PLAN_ID" NUMBER(20,0), 
	"CREATE_DATE" DATE NOT NULL ENABLE, 
	"CREATE_USER" VARCHAR2(50 BYTE) NOT NULL ENABLE, 
	"UPDATE_DATE" DATE, 
	"UPDATE_USER" VARCHAR2(50 BYTE), 
	"LAT" FLOAT(126), 
	"LNG" FLOAT(126), 
	"STAFF_ID" NUMBER(20,0) NOT NULL ENABLE, 
	"IS_FULL_TIME" NUMBER(1,0), 
	"IS_REPEAT" NUMBER(1,0), 
	"REPEAT_TYPE" VARCHAR2(50 BYTE), 
	"FROM_TIME" DATE NOT NULL ENABLE, 
	"TO_TIME" DATE, 
	"MONDAY" NUMBER(1,0), 
	"TUESDAY" NUMBER(1,0), 
	"WEDNESDAY" NUMBER(1,0), 
	"THURSDAY" NUMBER(1,0), 
	"FRIDAY" NUMBER(1,0), 
	"SATURDAY" NUMBER(1,0), 
	"SUNDAY" NUMBER(1,0), 
	"INTERVAL" NUMBER(20,0), 
	"MAX_NUMBER_INTERVAL" NUMBER(20,0), 
	"STATUS" NUMBER(1,0), 
	"FROM_DATE" DATE NOT NULL ENABLE, 
	"TO_DATE" DATE, 
	 CONSTRAINT "EVENT_ID_PK" PRIMARY KEY ("EVENT_ID"));
   
   COMMENT ON COLUMN "HABECO_RELEASE"."EVENT"."EVENT_TYPE" IS 'Lay CODE tu bang AP_PARAM tuong ung voi type = ''EVENT_TYPE''';
 
   COMMENT ON COLUMN "HABECO_RELEASE"."EVENT"."TRAINING_PLAN_ID" IS 'Bỏ';
 
   COMMENT ON COLUMN "HABECO_RELEASE"."EVENT"."IS_FULL_TIME" IS '1: Cả ngày';
 
   COMMENT ON COLUMN "HABECO_RELEASE"."EVENT"."IS_REPEAT" IS '1: Lặp lại';
 
   COMMENT ON COLUMN "HABECO_RELEASE"."EVENT"."REPEAT_TYPE" IS 'Lay CODE tu bang AP_PARAM tuong ung voi type = ''REPEAT_TYPE''';
 
   COMMENT ON COLUMN "HABECO_RELEASE"."EVENT"."FROM_TIME" IS 'Thời gian bắt đầu thực hiện';
 
   COMMENT ON COLUMN "HABECO_RELEASE"."EVENT"."TO_TIME" IS 'Thời gian kết thúc sự kiện';
 
   COMMENT ON COLUMN "HABECO_RELEASE"."EVENT"."INTERVAL" IS 'Repeat_Type = 0: ngày, 1: tuần';
 
   COMMENT ON COLUMN "HABECO_RELEASE"."EVENT"."MAX_NUMBER_INTERVAL" IS 'Số lần lặp tối đa';
 
   COMMENT ON COLUMN "HABECO_RELEASE"."EVENT"."STATUS" IS '0: Xoá';
 
   COMMENT ON COLUMN "HABECO_RELEASE"."EVENT"."FROM_DATE" IS 'Ngày bắt đầu hiệu lực
';
 
   COMMENT ON COLUMN "HABECO_RELEASE"."EVENT"."TO_DATE" IS 'Ngày hết hiệu lực';
   
   CREATE INDEX "HABECO_RELEASE"."E_FROM_DATE_IDX" ON "HABECO_RELEASE"."EVENT" ("FROM_DATE");
   CREATE INDEX "HABECO_RELEASE"."E_STAFF_ID_IDX" ON "HABECO_RELEASE"."EVENT" ("STAFF_ID");
   
   CREATE OR REPLACE TRIGGER "HABECO_RELEASE"."EVENT_TRIG" 
	AFTER INSERT OR DELETE OR UPDATE ON EVENT
	FOR EACH ROW
	DECLARE
	  v_staff_id STAFF_POSITION_LOG.staff_id%TYPE;
	  v_action database_log.action%TYPE;
	  v_table_id database_log.table_id%TYPE;
	BEGIN
		IF INSERTING THEN 
		  v_table_id:=:new.EVENT_ID;
		  v_staff_id:=:new.staff_id;
		  v_action:=1;
		ELSIF UPDATING THEN
		  v_table_id:=:new.EVENT_ID;
		  v_staff_id:=:new.staff_id;
		  v_action:=2;
		ELSIF DELETING THEN
		  v_table_id:=:old.EVENT_ID;
		  v_staff_id:=:old.staff_id;
		  v_action:=3;
		END IF;
	insert into database_log (database_log_id,table_name,table_id, action,  staff_id, shop_id, create_date) values
		(database_log_seq.nextval,'EVENT',v_table_id,v_action,v_staff_id,null,sysdate);
END;				 
				
/
ALTER TRIGGER "HABECO_RELEASE"."EVENT_TRIG" ENABLE;

-----b. Them bang EVENT_LOG
CREATE TABLE "HABECO_RELEASE"."EVENT_LOG" 
   (	"EVENT_LOG_ID" NUMBER(20,0), 
	"EVENT_ID" NUMBER(20,0) NOT NULL ENABLE, 
	"STAFF_ID" NUMBER(20,0) NOT NULL ENABLE, 
	"START_TIME" DATE NOT NULL ENABLE, 
	"END_TIME" DATE, 
	"START_LAT" FLOAT(126), 
	"START_LNG" FLOAT(126), 
	"END_LAT" FLOAT(126), 
	"END_LNG" FLOAT(126), 
	 CONSTRAINT "EVENT_LOG_ID_PK" PRIMARY KEY ("EVENT_LOG_ID"));
   
   CREATE INDEX "HABECO_RELEASE"."EL_EVENT_ID_IDX" ON "HABECO_RELEASE"."EVENT_LOG" ("EVENT_ID");
   CREATE INDEX "HABECO_RELEASE"."EL_FROM_DATE_IDX" ON "HABECO_RELEASE"."EVENT_LOG" ("START_TIME");
   CREATE INDEX "HABECO_RELEASE"."EL_STAFF_ID_IDX" ON "HABECO_RELEASE"."EVENT_LOG" ("STAFF_ID");
   
   CREATE OR REPLACE TRIGGER "HABECO_RELEASE"."EVENT_LOG_TRIG" 
	AFTER INSERT OR DELETE OR UPDATE ON EVENT_LOG
	FOR EACH ROW
	DECLARE
	  v_staff_id STAFF_POSITION_LOG.staff_id%TYPE;
	  v_action database_log.action%TYPE;
	  v_table_id database_log.table_id%TYPE;
	BEGIN
		IF INSERTING THEN 
		  v_table_id:=:new.EVENT_LOG_ID;
		  v_staff_id:=:new.staff_id;
		  v_action:=1;
		ELSIF UPDATING THEN
		  v_table_id:=:new.EVENT_LOG_ID;
		  v_staff_id:=:new.staff_id;
		  v_action:=2;
		ELSIF DELETING THEN
		  v_table_id:=:old.EVENT_LOG_ID;
		  v_staff_id:=:old.staff_id;
		  v_action:=3;
		END IF;
	insert into database_log (database_log_id,table_name,table_id, action,  staff_id, shop_id, create_date) values
		(database_log_seq.nextval,'EVENT_LOG',v_table_id,v_action,v_staff_id,null,sysdate);
END;				 
				
/
ALTER TRIGGER "HABECO_RELEASE"."EVENT_LOG_TRIG" ENABLE;

-----c. Them bang EVENT_NOTIFICATION
CREATE TABLE "HABECO_RELEASE"."EVENT_NOTIFICATION" 
   (	"EVENT_NOTIFICATION_ID" NUMBER(20,0), 
	"EVENT_ID" NUMBER(20,0) NOT NULL ENABLE, 
	"EVENT_NOTIFICATION_TYPE" NUMBER(2,0) NOT NULL ENABLE, 
	"TIME" TIMESTAMP (6) NOT NULL ENABLE, 
	"CREATE_DATE" DATE NOT NULL ENABLE, 
	"CREATE_USER" VARCHAR2(50 BYTE) NOT NULL ENABLE, 
	"UPDATE_DATE" DATE, 
	"UPDATE_USER" VARCHAR2(50 BYTE), 
	"CREATE_USER_ID" NUMBER(20,0), 
	 CONSTRAINT "NOTIFICATION_ID_PK" PRIMARY KEY ("EVENT_NOTIFICATION_ID"));
   
   COMMENT ON COLUMN "HABECO_RELEASE"."EVENT_NOTIFICATION"."EVENT_NOTIFICATION_TYPE" IS 'Lay CODE tu bang AP_PARAM tuong ung voi type = ''EVENT_NOTIFICATION_TYPE''';
 
   COMMENT ON COLUMN "HABECO_RELEASE"."EVENT_NOTIFICATION"."CREATE_USER_ID" IS 'id nguoi tao';
   
   CREATE INDEX "HABECO_RELEASE"."EN_EVENT_ID_IDX" ON "HABECO_RELEASE"."EVENT_NOTIFICATION" ("EVENT_ID");
   
   CREATE OR REPLACE TRIGGER "HABECO_RELEASE"."EVENT_NOTIFICATION_TRIG" 
	AFTER INSERT OR DELETE OR UPDATE ON EVENT_NOTIFICATION
	FOR EACH ROW
	DECLARE
	  v_staff_id STAFF_POSITION_LOG.staff_id%TYPE;
	  v_action database_log.action%TYPE;
	  v_table_id database_log.table_id%TYPE;
	BEGIN
		IF INSERTING THEN 
		  v_table_id:=:new.EVENT_NOTIFICATION_ID;
		  v_action:=1;
		ELSIF UPDATING THEN
		  v_table_id:=:new.EVENT_NOTIFICATION_ID;
		  v_action:=2;
		ELSIF DELETING THEN
		  v_table_id:=:old.EVENT_NOTIFICATION_ID;
		  v_action:=3;
		END IF;
	insert into database_log (database_log_id,table_name,table_id, action,  staff_id, shop_id, create_date) values
		(database_log_seq.nextval,'EVENT_NOTIFICATION',v_table_id,v_action,v_staff_id,NULL,sysdate);
END;				 
				
/
ALTER TRIGGER "HABECO_RELEASE"."EVENT_NOTIFICATION_TRIG" ENABLE;

-----d. Them bang global temporary EVENT_TEMP
CREATE GLOBAL TEMPORARY TABLE "HABECO_RELEASE"."EVENT_TEMP" 
   (	"EVENTID" NUMBER(20,0), 
	"EVENTNAME" VARCHAR2(40 BYTE) NOT NULL ENABLE, 
	"EVENTDESCRIPTION" VARCHAR2(200 BYTE), 
	"EVENTTYPE" VARCHAR2(50 BYTE) NOT NULL ENABLE, 
	"ADDRESS" VARCHAR2(200 BYTE), 
	"AREAID" NUMBER(20,0), 
	"TRAININGPLANID" NUMBER(20,0), 
	"CREATEDATE" DATE NOT NULL ENABLE, 
	"CREATEUSER" VARCHAR2(50 BYTE) NOT NULL ENABLE, 
	"UPDATEDATE" DATE, 
	"UPDATEUSER" VARCHAR2(50 BYTE), 
	"LAT" FLOAT(126), 
	"LNG" FLOAT(126), 
	"STAFFID" NUMBER(20,0) NOT NULL ENABLE, 
	"ISFULLTIME" NUMBER(1,0), 
	"ISREPEAT" NUMBER(1,0), 
	"REPEATTYPE" VARCHAR2(50 BYTE), 
	"FROMTIME" TIMESTAMP (6) NOT NULL ENABLE, 
	"TOTIME" TIMESTAMP (6), 
	"MONDAY" NUMBER(1,0), 
	"TUESDAY" NUMBER(1,0), 
	"WEDNESDAY" NUMBER(1,0), 
	"THURSDAY" NUMBER(1,0), 
	"FRIDAY" NUMBER(1,0), 
	"SATURDAY" NUMBER(1,0), 
	"SUNDAY" NUMBER(1,0), 
	"INTERVAL" NUMBER(20,0), 
	"MAXNUMBERINTERVAL" NUMBER(20,0), 
	"STATUS" NUMBER(1,0), 
	"FROMDATE" DATE NOT NULL ENABLE, 
	"TODATE" DATE, 
	"NUMBERDATE" NUMBER(20,0), 
	"FROMTIMENEW" TIMESTAMP (6), 
	"TOTIMENEW" TIMESTAMP (6), 
	"PROCESSTYPE" NUMBER(1,0), 
	"REPEATCOUNT" NUMBER(20,0)
   ) ON COMMIT DELETE ROWS ;
 

   COMMENT ON COLUMN "HABECO_RELEASE"."EVENT_TEMP"."PROCESSTYPE" IS '0: chưa thực hiện,1: đã thực hiện, 2: đang thực hiện';

-----e. Them bang sequence cho cac bang vua them
CREATE SEQUENCE  "HABECO_RELEASE"."EVENT_SEQ"  MINVALUE 1 MAXVALUE 999999999999999999999999999 INCREMENT BY 1 START WITH 1 CACHE 20 ORDER  NOCYCLE ;
CREATE SEQUENCE  "HABECO_RELEASE"."EVENT_LOG_SEQ"  MINVALUE 1 MAXVALUE 999999999999999999999999999 INCREMENT BY 1 START WITH 1 CACHE 20 ORDER  NOCYCLE ;
CREATE SEQUENCE  "HABECO_RELEASE"."EVENT_NOTIFICATION_SEQ"  MINVALUE 1 MAXVALUE 999999999999999999999999999 INCREMENT BY 1 START WITH 1 CACHE 20 ORDER  NOCYCLE ;

-----f. Them cac dong sau vao ap_param
Insert into AP_PARAM (AP_PARAM_ID,AP_PARAM_CODE,AP_PARAM_NAME,VALUE,STATUS,TYPE,DESCRIPTION) values (AP_PARAM_SEQ.NEXTVAL,'1','Công tác','1',1,'EVENT_TYPE',null);
Insert into AP_PARAM (AP_PARAM_ID,AP_PARAM_CODE,AP_PARAM_NAME,VALUE,STATUS,TYPE,DESCRIPTION) values (AP_PARAM_SEQ.NEXTVAL,'2','Huấn luyện','2',1,'EVENT_TYPE',null);
Insert into AP_PARAM (AP_PARAM_ID,AP_PARAM_CODE,AP_PARAM_NAME,VALUE,STATUS,TYPE,DESCRIPTION) values (AP_PARAM_SEQ.NEXTVAL,'0','Công việc','0',1,'EVENT_TYPE',null);
Insert into AP_PARAM (AP_PARAM_ID,AP_PARAM_CODE,AP_PARAM_NAME,VALUE,STATUS,TYPE,DESCRIPTION) values (AP_PARAM_SEQ.NEXTVAL,'0','Hàng ngày',null,1,'REPEAT_TYPE',null);
Insert into AP_PARAM (AP_PARAM_ID,AP_PARAM_CODE,AP_PARAM_NAME,VALUE,STATUS,TYPE,DESCRIPTION) values (AP_PARAM_SEQ.NEXTVAL,'1','Hàng tuần',null,1,'REPEAT_TYPE',null);
Insert into AP_PARAM (AP_PARAM_ID,AP_PARAM_CODE,AP_PARAM_NAME,VALUE,STATUS,TYPE,DESCRIPTION) values (AP_PARAM_SEQ.NEXTVAL,'3','Hàng năm',null,0,'REPEAT_TYPE',null);
Insert into AP_PARAM (AP_PARAM_ID,AP_PARAM_CODE,AP_PARAM_NAME,VALUE,STATUS,TYPE,DESCRIPTION) values (AP_PARAM_SEQ.NEXTVAL,'2','Hàng tháng',null,0,'REPEAT_TYPE',null);
Insert into AP_PARAM (AP_PARAM_ID,AP_PARAM_CODE,AP_PARAM_NAME,VALUE,STATUS,TYPE,DESCRIPTION) values (AP_PARAM_SEQ.NEXTVAL,'0','Cửa sổ bật lên','0',1,'EVENT_NOTIFICATION_TYPE',null);
Insert into AP_PARAM (AP_PARAM_ID,AP_PARAM_CODE,AP_PARAM_NAME,VALUE,STATUS,TYPE,DESCRIPTION) values (AP_PARAM_SEQ.NEXTVAL,'1','Email','1',1,'EVENT_NOTIFICATION_TYPE',null);
Insert into AP_PARAM (AP_PARAM_ID,AP_PARAM_CODE,AP_PARAM_NAME,VALUE,STATUS,TYPE,DESCRIPTION) values (AP_PARAM_SEQ.NEXTVAL,'2','Tin nhắn','2',1,'EVENT_NOTIFICATION_TYPE',null);
commit;

-----g. Them cot EVENT_ID cho bang TRAINING_PLAN_DETAIL
alter table TRAINING_PLAN_DETAIL add(EVENT_ID NUMBER(20,0));

-----h. Them package header PKG_TRUNG
create or replace PACKAGE PKG_TRUNG AS 

  /* TODO enter package declarations (types, exceptions, methods etc) here */ 
  PROCEDURE P_GET_EVENT_SCHEDULE(res OUT SYS_REFCURSOR, p_StaffId number, p_FromDate Date, p_ToDate Date);

END PKG_TRUNG;

-----i. Them package body PKG_TRUNG
create or replace PACKAGE BODY PKG_TRUNG AS

  PROCEDURE P_GET_EVENT_SCHEDULE(res OUT SYS_REFCURSOR, p_StaffId number, p_FromDate Date, p_ToDate Date) AS
  cursor c_event is
    select * from event
    where staff_id = p_StaffId and ((trunc(p_FromDate) <= from_date
        and trunc(p_ToDate) >= trunc(from_date)) and from_date <= to_date
      or (trunc(p_FromDate) >= from_date and (to_date is null or trunc(p_FromDate) <= to_date)))
        and (status is null or status = 1);
  v_from_date_temp date := null;
  v_from_date date := null;
  v_to_date date := null;
  v_from_time date := null;
  v_to_time date := null;
  v_end_time date := null;
  v_process_type number := null;
  v_count number := 0;

  BEGIN
    -- TODO: Implementation required for PROCEDURE PKG_TRUNG.P_GET_EVENT_SCHEDULE
    for v_event in c_event
    loop
      if (v_event.is_repeat is null or v_event.is_repeat = 0) then
        v_process_type := 1;
        v_end_time := null;
        begin
          select end_time into v_end_time
          from event_log where event_id = v_event.event_id and start_time >= trunc(v_event.from_time)
            and start_time < trunc(v_event.to_time + 1) and staff_id = v_event.staff_id;
        EXCEPTION WHEN NO_DATA_FOUND THEN v_process_type := 0;
        end;
        if v_end_time is not null then
          v_process_type := 2;
        end if;

        Insert into EVENT_TEMP (EVENTID,EVENTNAME,EVENTDESCRIPTION,EVENTTYPE,ADDRESS,AREAID
          ,TRAININGPLANID,CREATEDATE,CREATEUSER,UPDATEDATE,UPDATEUSER,LAT
          ,LNG,STAFFID,ISFULLTIME,ISREPEAT,REPEATTYPE,FROMTIME,TOTIME
          ,MONDAY,TUESDAY,WEDNESDAY,THURSDAY,FRIDAY,SATURDAY,SUNDAY
          ,INTERVAL,MAXNUMBERINTERVAL,STATUS,FROMDATE,TODATE
          ,NUMBERDATE, FROMTIMENEW, TOTIMENEW, PROCESSTYPE,REPEATCOUNT)
        values(v_event.event_id,v_event.event_name,v_event.event_description,v_event.event_type,v_event.address,v_event.area_id
          ,v_event.training_plan_id,v_event.create_date,v_event.create_user,v_event.update_date,v_event.update_user,v_event.lat
          ,v_event.lng,v_event.staff_id,v_event.is_full_time,v_event.is_repeat,v_event.repeat_type,v_event.from_time,v_event.to_time
          ,v_event.monday,v_event.tuesday,v_event.wednesday,v_event.thursday,v_event.friday,v_event.saturday,v_event.sunday
          ,v_event.interval,v_event.max_number_interval,v_event.status,v_event.from_date,v_event.to_date
          ,trunc(v_event.to_time) - trunc(v_event.from_time),v_event.from_time,trunc(v_event.to_time + 1), v_process_type,1);
      elsif v_event.is_repeat = 1 then
        v_from_date_temp := trunc(F_MAX_DATE(p_FromDate,v_event.from_date));
        v_to_date := trunc(F_MIN_DATE(p_ToDate,v_event.to_date));
        v_count := 0;
        if v_event.repeat_type = 0 then
          if(v_event.max_number_interval is null) then
            v_from_date := mod(trunc(v_from_date_temp) - trunc(v_event.from_date),v_event.interval) + v_from_date_temp;
          else
            v_from_date := v_event.from_date;
          end if;
          while (v_to_date is null or v_from_date <= v_to_date) and (v_event.max_number_interval is null or v_count <= v_event.max_number_interval)
          loop
            v_count := v_count + 1;
            v_from_time := to_date(to_char(v_from_date,'dd/mm/yyyy') || ' ' || to_char(v_event.from_time,'hh24:mi:ss'),'dd/mm/yyyy hh24:mi:ss');
            v_to_time := to_date(to_char(v_from_time + (trunc(v_event.to_time) - trunc(v_event.from_time)),'dd/mm/yyyy') || ' ' || to_char(v_event.to_time,'hh24:mi:ss'),'dd/mm/yyyy hh24:mi:ss');
            v_process_type := 2; ---chuyen doi tu 2 ->  1 : Dang thuc hien
            v_end_time := null;
            begin
              select end_time into v_end_time
              from event_log where event_id = v_event.event_id and start_time >= trunc(v_from_time)
                and start_time < trunc(v_to_time + 1) and staff_id = v_event.staff_id;
            EXCEPTION WHEN NO_DATA_FOUND THEN v_process_type := 0;
            end;
            if v_end_time is not null then
              v_process_type := 1;   ---chuyen doi tu 1 ->2: Da thuc hien
            end if;

            if v_from_date >= v_from_date_temp and (v_event.max_number_interval is null or v_count <= v_event.max_number_interval) then
              Insert into EVENT_TEMP (EVENTID,EVENTNAME,EVENTDESCRIPTION,EVENTTYPE,ADDRESS,AREAID
                ,TRAININGPLANID,CREATEDATE,CREATEUSER,UPDATEDATE,UPDATEUSER,LAT
                ,LNG,STAFFID,ISFULLTIME,ISREPEAT,REPEATTYPE,FROMTIME,TOTIME
                ,MONDAY,TUESDAY,WEDNESDAY,THURSDAY,FRIDAY,SATURDAY,SUNDAY
                ,INTERVAL,MAXNUMBERINTERVAL,STATUS,FROMDATE,TODATE
                ,NUMBERDATE, FROMTIMENEW, TOTIMENEW, PROCESSTYPE,REPEATCOUNT)
              values(v_event.event_id,v_event.event_name,v_event.event_description,v_event.event_type,v_event.address,v_event.area_id
                ,v_event.training_plan_id,v_event.create_date,v_event.create_user,v_event.update_date,v_event.update_user,v_event.lat
                ,v_event.lng,v_event.staff_id,v_event.is_full_time,v_event.is_repeat,v_event.repeat_type,v_event.from_time,v_event.to_time
                ,v_event.monday,v_event.tuesday,v_event.wednesday,v_event.thursday,v_event.friday,v_event.saturday,v_event.sunday
                ,v_event.interval,v_event.max_number_interval,v_event.status,v_event.from_date,v_event.to_date
                ,trunc(v_event.to_time) - trunc(v_event.from_time), v_from_time, trunc(v_to_time + 1), v_process_type,v_count);
            end if;
            v_from_date := v_from_date + v_event.interval;
          end loop;
        elsif v_event.repeat_type = 1 then
          if(v_event.max_number_interval is null) then
            v_from_date := mod(trunc(v_from_date_temp,'iw') - trunc(v_event.from_date,'iw'),v_event.interval)*7 + trunc(v_from_date_temp,'iw');
          else
            v_from_date := TRUNC(v_event.from_date, 'iw');
          end if;
          while (v_to_date is null or v_from_date <= v_to_date) and (v_event.max_number_interval is null or v_count <= v_event.max_number_interval)
          loop
            --thu hai
            if v_event.monday is not null and v_event.monday = 1 and (v_to_date is null or v_from_date <= v_to_date) and v_from_date >= v_event.from_date then
              v_count := v_count + 1;
              v_from_time := to_date(to_char(v_from_date + 0,'dd/mm/yyyy') || ' ' || to_char(v_event.from_time,'hh24:mi:ss'),'dd/mm/yyyy hh24:mi:ss');
              v_to_time := to_date(to_char(v_from_time + (trunc(v_event.to_time) - trunc(v_event.from_time)),'dd/mm/yyyy') || ' ' || to_char(v_event.to_time,'hh24:mi:ss'),'dd/mm/yyyy hh24:mi:ss');
              v_process_type := 1;
              v_end_time := null;
              begin
                select end_time into v_end_time
                from event_log where event_id = v_event.event_id and start_time >= trunc(v_from_time)
                  and start_time < trunc(v_to_time + 1) and staff_id = v_event.staff_id;
              EXCEPTION WHEN NO_DATA_FOUND THEN v_process_type := 0;
              end;
              if v_end_time is not null then
                v_process_type := 2;
              end if;

              if v_from_date >= trunc(v_from_date_temp) and (v_event.max_number_interval is null or v_count <= v_event.max_number_interval) then
                Insert into EVENT_TEMP (EVENTID,EVENTNAME,EVENTDESCRIPTION,EVENTTYPE,ADDRESS,AREAID
                  ,TRAININGPLANID,CREATEDATE,CREATEUSER,UPDATEDATE,UPDATEUSER,LAT
                  ,LNG,STAFFID,ISFULLTIME,ISREPEAT,REPEATTYPE,FROMTIME,TOTIME
                  ,MONDAY,TUESDAY,WEDNESDAY,THURSDAY,FRIDAY,SATURDAY,SUNDAY
                  ,INTERVAL,MAXNUMBERINTERVAL,STATUS,FROMDATE,TODATE
                  ,NUMBERDATE, FROMTIMENEW, TOTIMENEW, PROCESSTYPE,REPEATCOUNT)
                values(v_event.event_id,v_event.event_name,v_event.event_description,v_event.event_type,v_event.address,v_event.area_id
                  ,v_event.training_plan_id,v_event.create_date,v_event.create_user,v_event.update_date,v_event.update_user,v_event.lat
                  ,v_event.lng,v_event.staff_id,v_event.is_full_time,v_event.is_repeat,v_event.repeat_type,v_event.from_time,v_event.to_time
                  ,v_event.monday,v_event.tuesday,v_event.wednesday,v_event.thursday,v_event.friday,v_event.saturday,v_event.sunday
                  ,v_event.interval,v_event.max_number_interval,v_event.status,v_event.from_date,v_event.to_date
                  ,trunc(v_event.to_time) - trunc(v_event.from_time), v_from_time, trunc(v_to_time + 1), v_process_type,v_count);
              end if;
            end if;

            --thu ba
            if v_event.tuesday is not null and v_event.tuesday = 1 and (v_to_date is null or v_from_date + 1 <= v_to_date) and v_from_date + 1 >= v_event.from_date then
              v_count := v_count + 1;
              v_from_time := to_date(to_char(v_from_date + 1,'dd/mm/yyyy') || ' ' || to_char(v_event.from_time,'hh24:mi:ss'),'dd/mm/yyyy hh24:mi:ss');
              v_to_time := to_date(to_char(v_from_time + (trunc(v_event.to_time) - trunc(v_event.from_time)),'dd/mm/yyyy') || ' ' || to_char(v_event.to_time,'hh24:mi:ss'),'dd/mm/yyyy hh24:mi:ss');
              v_process_type := 1;
              v_end_time := null;
              begin
                select end_time into v_end_time
                from event_log where event_id = v_event.event_id and start_time >= trunc(v_from_time)
                  and start_time < trunc(v_to_time + 1) and staff_id = v_event.staff_id;
              EXCEPTION WHEN NO_DATA_FOUND THEN v_process_type := 0;
              end;
              if v_end_time is not null then
                v_process_type := 2;
              end if;

              if v_from_date + 1 >= trunc(v_from_date_temp) and (v_event.max_number_interval is null or v_count <= v_event.max_number_interval) then
                Insert into EVENT_TEMP (EVENTID,EVENTNAME,EVENTDESCRIPTION,EVENTTYPE,ADDRESS,AREAID
                  ,TRAININGPLANID,CREATEDATE,CREATEUSER,UPDATEDATE,UPDATEUSER,LAT
                  ,LNG,STAFFID,ISFULLTIME,ISREPEAT,REPEATTYPE,FROMTIME,TOTIME
                  ,MONDAY,TUESDAY,WEDNESDAY,THURSDAY,FRIDAY,SATURDAY,SUNDAY
                  ,INTERVAL,MAXNUMBERINTERVAL,STATUS,FROMDATE,TODATE
                  ,NUMBERDATE, FROMTIMENEW, TOTIMENEW, PROCESSTYPE,REPEATCOUNT)
                values(v_event.event_id,v_event.event_name,v_event.event_description,v_event.event_type,v_event.address,v_event.area_id
                  ,v_event.training_plan_id,v_event.create_date,v_event.create_user,v_event.update_date,v_event.update_user,v_event.lat
                  ,v_event.lng,v_event.staff_id,v_event.is_full_time,v_event.is_repeat,v_event.repeat_type,v_event.from_time,v_event.to_time
                  ,v_event.monday,v_event.tuesday,v_event.wednesday,v_event.thursday,v_event.friday,v_event.saturday,v_event.sunday
                  ,v_event.interval,v_event.max_number_interval,v_event.status,v_event.from_date,v_event.to_date
                  ,trunc(v_event.to_time) - trunc(v_event.from_time), v_from_time, trunc(v_to_time + 1), v_process_type,v_count);
              end if;
            end if;

            --thu tu
            if v_event.wednesday is not null and v_event.wednesday = 1 and (v_to_date is null or v_from_date + 2 <= v_to_date) and v_from_date + 2 >= v_event.from_date then
              v_count := v_count + 1;
              v_from_time := to_date(to_char(v_from_date + 2,'dd/mm/yyyy') || ' ' || to_char(v_event.from_time,'hh24:mi:ss'),'dd/mm/yyyy hh24:mi:ss');
              v_to_time := to_date(to_char(v_from_time + (trunc(v_event.to_time) - trunc(v_event.from_time)),'dd/mm/yyyy') || ' ' || to_char(v_event.to_time,'hh24:mi:ss'),'dd/mm/yyyy hh24:mi:ss');
              v_process_type := 1;
              v_end_time := null;
              begin
                select end_time into v_end_time
                from event_log where event_id = v_event.event_id and start_time >= trunc(v_from_time)
                  and start_time < trunc(v_to_time + 1) and staff_id = v_event.staff_id;
              EXCEPTION WHEN NO_DATA_FOUND THEN v_process_type := 0;
              end;
              if v_end_time is not null then
                v_process_type := 2;
              end if;

              if v_from_date + 2 >= trunc(v_from_date_temp) and (v_event.max_number_interval is null or v_count <= v_event.max_number_interval) then
                Insert into EVENT_TEMP (EVENTID,EVENTNAME,EVENTDESCRIPTION,EVENTTYPE,ADDRESS,AREAID
                  ,TRAININGPLANID,CREATEDATE,CREATEUSER,UPDATEDATE,UPDATEUSER,LAT
                  ,LNG,STAFFID,ISFULLTIME,ISREPEAT,REPEATTYPE,FROMTIME,TOTIME
                  ,MONDAY,TUESDAY,WEDNESDAY,THURSDAY,FRIDAY,SATURDAY,SUNDAY
                  ,INTERVAL,MAXNUMBERINTERVAL,STATUS,FROMDATE,TODATE
                  ,NUMBERDATE, FROMTIMENEW, TOTIMENEW, PROCESSTYPE,REPEATCOUNT)
                values(v_event.event_id,v_event.event_name,v_event.event_description,v_event.event_type,v_event.address,v_event.area_id
                  ,v_event.training_plan_id,v_event.create_date,v_event.create_user,v_event.update_date,v_event.update_user,v_event.lat
                  ,v_event.lng,v_event.staff_id,v_event.is_full_time,v_event.is_repeat,v_event.repeat_type,v_event.from_time,v_event.to_time
                  ,v_event.monday,v_event.tuesday,v_event.wednesday,v_event.thursday,v_event.friday,v_event.saturday,v_event.sunday
                  ,v_event.interval,v_event.max_number_interval,v_event.status,v_event.from_date,v_event.to_date
                  ,trunc(v_event.to_time) - trunc(v_event.from_time), v_from_time, trunc(v_to_time + 1), v_process_type,v_count);
              end if;
            end if;

            --thu nam
            if v_event.thursday is not null and v_event.thursday = 1 and (v_to_date is null or v_from_date + 3 <= v_to_date) and v_from_date + 3 >= v_event.from_date then
              v_count := v_count + 1;
              v_from_time := to_date(to_char(v_from_date + 3,'dd/mm/yyyy') || ' ' || to_char(v_event.from_time,'hh24:mi:ss'),'dd/mm/yyyy hh24:mi:ss');
              v_to_time := to_date(to_char(v_from_time + (trunc(v_event.to_time) - trunc(v_event.from_time)),'dd/mm/yyyy') || ' ' || to_char(v_event.to_time,'hh24:mi:ss'),'dd/mm/yyyy hh24:mi:ss');
              v_process_type := 1;
              v_end_time := null;
              begin
                select end_time into v_end_time
                from event_log where event_id = v_event.event_id and start_time >= trunc(v_from_time)
                  and start_time < trunc(v_to_time + 1) and staff_id = v_event.staff_id;
              EXCEPTION WHEN NO_DATA_FOUND THEN v_process_type := 0;
              end;
              if v_end_time is not null then
                v_process_type := 2;
              end if;

              if v_from_date + 3 >= trunc(v_from_date_temp) and (v_event.max_number_interval is null or v_count <= v_event.max_number_interval) then
                Insert into EVENT_TEMP (EVENTID,EVENTNAME,EVENTDESCRIPTION,EVENTTYPE,ADDRESS,AREAID
                  ,TRAININGPLANID,CREATEDATE,CREATEUSER,UPDATEDATE,UPDATEUSER,LAT
                  ,LNG,STAFFID,ISFULLTIME,ISREPEAT,REPEATTYPE,FROMTIME,TOTIME
                  ,MONDAY,TUESDAY,WEDNESDAY,THURSDAY,FRIDAY,SATURDAY,SUNDAY
                  ,INTERVAL,MAXNUMBERINTERVAL,STATUS,FROMDATE,TODATE
                  ,NUMBERDATE, FROMTIMENEW, TOTIMENEW, PROCESSTYPE,REPEATCOUNT)
                values(v_event.event_id,v_event.event_name,v_event.event_description,v_event.event_type,v_event.address,v_event.area_id
                  ,v_event.training_plan_id,v_event.create_date,v_event.create_user,v_event.update_date,v_event.update_user,v_event.lat
                  ,v_event.lng,v_event.staff_id,v_event.is_full_time,v_event.is_repeat,v_event.repeat_type,v_event.from_time,v_event.to_time
                  ,v_event.monday,v_event.tuesday,v_event.wednesday,v_event.thursday,v_event.friday,v_event.saturday,v_event.sunday
                  ,v_event.interval,v_event.max_number_interval,v_event.status,v_event.from_date,v_event.to_date
                  ,trunc(v_event.to_time) - trunc(v_event.from_time), v_from_time, trunc(v_to_time + 1), v_process_type,v_count);
              end if;
            end if;

            --thu sau
            if v_event.friday is not null and v_event.friday = 1 and (v_to_date is null or v_from_date + 4 <= v_to_date) and v_from_date + 4 >= v_event.from_date then
              v_count := v_count + 1;
              v_from_time := to_date(to_char(v_from_date + 4,'dd/mm/yyyy') || ' ' || to_char(v_event.from_time,'hh24:mi:ss'),'dd/mm/yyyy hh24:mi:ss');
              v_to_time := to_date(to_char(v_from_time + (trunc(v_event.to_time) - trunc(v_event.from_time)),'dd/mm/yyyy') || ' ' || to_char(v_event.to_time,'hh24:mi:ss'),'dd/mm/yyyy hh24:mi:ss');
              v_process_type := 1;
              v_end_time := null;
              begin
                select end_time into v_end_time
                from event_log where event_id = v_event.event_id and start_time >= trunc(v_from_time)
                  and start_time < trunc(v_to_time + 1) and staff_id = v_event.staff_id;
              EXCEPTION WHEN NO_DATA_FOUND THEN v_process_type := 0;
              end;
              if v_end_time is not null then
                v_process_type := 2;
              end if;

              if v_from_date + 4 >= trunc(v_from_date_temp) and (v_event.max_number_interval is null or v_count <= v_event.max_number_interval) then
                Insert into EVENT_TEMP (EVENTID,EVENTNAME,EVENTDESCRIPTION,EVENTTYPE,ADDRESS,AREAID
                  ,TRAININGPLANID,CREATEDATE,CREATEUSER,UPDATEDATE,UPDATEUSER,LAT
                  ,LNG,STAFFID,ISFULLTIME,ISREPEAT,REPEATTYPE,FROMTIME,TOTIME
                  ,MONDAY,TUESDAY,WEDNESDAY,THURSDAY,FRIDAY,SATURDAY,SUNDAY
                  ,INTERVAL,MAXNUMBERINTERVAL,STATUS,FROMDATE,TODATE
                  ,NUMBERDATE, FROMTIMENEW, TOTIMENEW, PROCESSTYPE,REPEATCOUNT)
                values(v_event.event_id,v_event.event_name,v_event.event_description,v_event.event_type,v_event.address,v_event.area_id
                  ,v_event.training_plan_id,v_event.create_date,v_event.create_user,v_event.update_date,v_event.update_user,v_event.lat
                  ,v_event.lng,v_event.staff_id,v_event.is_full_time,v_event.is_repeat,v_event.repeat_type,v_event.from_time,v_event.to_time
                  ,v_event.monday,v_event.tuesday,v_event.wednesday,v_event.thursday,v_event.friday,v_event.saturday,v_event.sunday
                  ,v_event.interval,v_event.max_number_interval,v_event.status,v_event.from_date,v_event.to_date
                  ,trunc(v_event.to_time) - trunc(v_event.from_time), v_from_time, trunc(v_to_time + 1), v_process_type,v_count);
              end if;
            end if;

            --thu bay
            if v_event.saturday is not null and v_event.saturday = 1 and (v_to_date is null or v_from_date + 5 <= v_to_date) and v_from_date + 5 >= v_event.from_date then
              v_count := v_count + 1;
              v_from_time := to_date(to_char(v_from_date + 5,'dd/mm/yyyy') || ' ' || to_char(v_event.from_time,'hh24:mi:ss'),'dd/mm/yyyy hh24:mi:ss');
              v_to_time := to_date(to_char(v_from_time + (trunc(v_event.to_time) - trunc(v_event.from_time)),'dd/mm/yyyy') || ' ' || to_char(v_event.to_time,'hh24:mi:ss'),'dd/mm/yyyy hh24:mi:ss');
              v_process_type := 1;
              v_end_time := null;
              begin
                select end_time into v_end_time
                from event_log where event_id = v_event.event_id and start_time >= trunc(v_from_time)
                  and start_time < trunc(v_to_time + 1) and staff_id = v_event.staff_id;
              EXCEPTION WHEN NO_DATA_FOUND THEN v_process_type := 0;
              end;
              if v_end_time is not null then
                v_process_type := 2;
              end if;

              if v_from_date + 5 >= trunc(v_from_date_temp) and (v_event.max_number_interval is null or v_count <= v_event.max_number_interval) then
                Insert into EVENT_TEMP (EVENTID,EVENTNAME,EVENTDESCRIPTION,EVENTTYPE,ADDRESS,AREAID
                  ,TRAININGPLANID,CREATEDATE,CREATEUSER,UPDATEDATE,UPDATEUSER,LAT
                  ,LNG,STAFFID,ISFULLTIME,ISREPEAT,REPEATTYPE,FROMTIME,TOTIME
                  ,MONDAY,TUESDAY,WEDNESDAY,THURSDAY,FRIDAY,SATURDAY,SUNDAY
                  ,INTERVAL,MAXNUMBERINTERVAL,STATUS,FROMDATE,TODATE
                  ,NUMBERDATE, FROMTIMENEW, TOTIMENEW, PROCESSTYPE,REPEATCOUNT)
                values(v_event.event_id,v_event.event_name,v_event.event_description,v_event.event_type,v_event.address,v_event.area_id
                  ,v_event.training_plan_id,v_event.create_date,v_event.create_user,v_event.update_date,v_event.update_user,v_event.lat
                  ,v_event.lng,v_event.staff_id,v_event.is_full_time,v_event.is_repeat,v_event.repeat_type,v_event.from_time,v_event.to_time
                  ,v_event.monday,v_event.tuesday,v_event.wednesday,v_event.thursday,v_event.friday,v_event.saturday,v_event.sunday
                  ,v_event.interval,v_event.max_number_interval,v_event.status,v_event.from_date,v_event.to_date
                  ,trunc(v_event.to_time) - trunc(v_event.from_time), v_from_time, trunc(v_to_time + 1), v_process_type,v_count);
              end if;
            end if;

            --chu nhat
            if v_event.sunday is not null and v_event.sunday = 1 and (v_to_date is null or v_from_date + 6 <= v_to_date) and v_from_date + 6 >= v_event.from_date then
              v_count := v_count + 1;
              v_from_time := to_date(to_char(v_from_date + 6,'dd/mm/yyyy') || ' ' || to_char(v_event.from_time,'hh24:mi:ss'),'dd/mm/yyyy hh24:mi:ss');
              v_to_time := to_date(to_char(v_from_time + (trunc(v_event.to_time) - trunc(v_event.from_time)),'dd/mm/yyyy') || ' ' || to_char(v_event.to_time,'hh24:mi:ss'),'dd/mm/yyyy hh24:mi:ss');
              v_process_type := 1;
              v_end_time := null;
              begin
                select end_time into v_end_time
                from event_log where event_id = v_event.event_id and start_time >= trunc(v_from_time)
                  and start_time < trunc(v_to_time + 1) and staff_id = v_event.staff_id;
              EXCEPTION WHEN NO_DATA_FOUND THEN v_process_type := 0;
              end;
              if v_end_time is not null then
                v_process_type := 2;
              end if;

              if v_from_date + 6 >= trunc(v_from_date_temp) and (v_event.max_number_interval is null or v_count <= v_event.max_number_interval) then
                Insert into EVENT_TEMP (EVENTID,EVENTNAME,EVENTDESCRIPTION,EVENTTYPE,ADDRESS,AREAID
                  ,TRAININGPLANID,CREATEDATE,CREATEUSER,UPDATEDATE,UPDATEUSER,LAT
                  ,LNG,STAFFID,ISFULLTIME,ISREPEAT,REPEATTYPE,FROMTIME,TOTIME
                  ,MONDAY,TUESDAY,WEDNESDAY,THURSDAY,FRIDAY,SATURDAY,SUNDAY
                  ,INTERVAL,MAXNUMBERINTERVAL,STATUS,FROMDATE,TODATE
                  ,NUMBERDATE, FROMTIMENEW, TOTIMENEW, PROCESSTYPE,REPEATCOUNT)
                values(v_event.event_id,v_event.event_name,v_event.event_description,v_event.event_type,v_event.address,v_event.area_id
                  ,v_event.training_plan_id,v_event.create_date,v_event.create_user,v_event.update_date,v_event.update_user,v_event.lat
                  ,v_event.lng,v_event.staff_id,v_event.is_full_time,v_event.is_repeat,v_event.repeat_type,v_event.from_time,v_event.to_time
                  ,v_event.monday,v_event.tuesday,v_event.wednesday,v_event.thursday,v_event.friday,v_event.saturday,v_event.sunday
                  ,v_event.interval,v_event.max_number_interval,v_event.status,v_event.from_date,v_event.to_date
                  ,trunc(v_event.to_time) - trunc(v_event.from_time), v_from_time, trunc(v_to_time + 1), v_process_type,v_count);
              end if;
            end if;
            v_from_date := v_from_date + v_event.interval*7;
          end loop;
        end if;
      end if;
    end loop;
    open res for
      select * from event_temp;
  END P_GET_EVENT_SCHEDULE;

END PKG_TRUNG;

----2. Chinh sua bao cao CRM 3.8
-----a. Chinh sua procedure P_RPT_STAFF_FAULT
create or replace PROCEDURE P_RPT_STAFF_FAULT (dateInput DATE)
-- @authOR: NaLD
-- Tien trinh chay 15p/lan
-- Tong hop vi pham cua NVBH trong ngay truyen vao dateInput
IS
  rptID NUMBER (20,0);
  checkinsert NUMBER(2,0);
  
  nppTime NUMBER(1);
  firstCusLate NUMBER(1);
  lastCusEarly NUMBER(1);
  routingTime NUMBER(1);
  CONST_ROUTINGTIME NUMBER(10) := 8;

  cc_start VARCHAR2(20);
  cc_end VARCHAR2(20);
  cc_distance VARCHAR2(20);
  dt_start VARCHAR2(20);
  dt_end VARCHAR2(20);
  npp_lat NUMBER(30, 20);
  npp_lng NUMBER(30, 20);
  v_start_time date := null;
  v_end_time date := null;
  v_from_time_rest_str varchar(20) := null;
  v_to_time_rest_str varchar(20) := null;
  v_from_time_rest date := null;
  v_to_time_rest date := null;
  v_time number := null;

  -- Lay ra tap NVKD
  CURSOR c_DuLieu
  IS
    SELECT st.staff_id, vp.shop_id
    FROM staff st 
    JOIN channel_type ct ON ct.channel_type_id = st.staff_type_id
    join visit_plan vp on vp.staff_id = st.staff_id and trunc(vp.from_date) <= trunc(dateInput)
      and (vp.to_date is null or vp.to_date >= trunc(dateInput))  and vp.status in (0,1)
    WHERE ct.type = 2 AND ct.object_type IN (1) -- NVKD, TTTT
      AND st.status in (0, 1)
      ;
  
  CURSOR c_DeleteData
  IS
    WITH
    dl AS(
      SELECT st.staff_id, vp.shop_id
    FROM staff st 
    JOIN channel_type ct ON ct.channel_type_id = st.staff_type_id
    join visit_plan vp on vp.staff_id = st.staff_id and trunc(vp.from_date) <= trunc(dateInput)
      and (vp.to_date is null or vp.to_date >= trunc(dateInput)) and vp.status in (0,1)
    WHERE ct.type = 2 AND ct.object_type IN (1) -- NVKD, TTTT
      AND st.status in (0, 1)
    )
    SELECT rpt.rpt_staff_fault_id 
    FROM rpt_staff_fault rpt 
    WHERE rpt.rpt_in_date >= TRUNC(dateInput) AND rpt.rpt_in_date < TRUNC(dateInput) + 1
    AND NOT EXISTS (SELECT 1 FROM dl WHERE dl.staff_id = rpt.staff_id AND dl.shop_id = rpt.shop_id)
    ;
  
  -- Lay ra tham so cau hinh SHOP_PARAM cua NPP dua vao id shop v_shopId
  CURSOR c_ShopParam (v_shopId NUMBER)
  IS
    SELECT
    (SELECT sp.code value
    FROM shop_param sp WHERE sp.shop_id = v_shopId AND sp.type = 'CC_START') cc_start
    ,(SELECT sp.code value
    FROM shop_param sp WHERE sp.shop_id = v_shopId AND sp.type = 'CC_END') cc_end
    ,(SELECT sp.code value
    FROM shop_param sp WHERE sp.shop_id = v_shopId AND sp.type = 'CC_DISTANCE') cc_distance
    ,(SELECT sp.code value
    FROM shop_param sp WHERE sp.shop_id = v_shopId AND sp.type = 'DT_START') dt_start
    ,(SELECT sp.code value
    FROM shop_param sp WHERE sp.shop_id = v_shopId AND sp.type = 'DT_END') dt_end
    ,(SELECT s.lat FROM shop s WHERE s.shop_id = v_shopId) npp_lat
    ,(SELECT s.lng FROM shop s WHERE s.shop_id = v_shopId) npp_lng
    FROM dual
    ;
  
BEGIN
  DBMS_OUTPUT.PUT_LINE('P_RPT_STAFF_FAULT started');
  FOR v IN c_DuLieu
  LOOP
    nppTime := 0;
    firstCusLate := 0;
    lastCusEarly := 0;
    routingTime := 0;
    v_time := 0;

    OPEN c_ShopParam(v.shop_id);
    FETCH c_ShopParam INTO cc_start, cc_end, cc_distance,dt_start,dt_end, npp_lat, npp_lng;
    CLOSE c_ShopParam;

    -- Tinh cho cot NPP_TIME
    if systimestamp > to_timestamp(to_char(dateInput, 'dd/mm/yyyy') || ' ' || to_char(to_date(cc_end, 'hh24:mi'), 'hh24:mi:ss'), 'dd/mm/yyyy HH24:MI:SS') then
      BEGIN
        SELECT spl.staff_position_log_id INTO rptID FROM staff_position_log spl
        WHERE spl.staff_id = v.staff_id AND spl.create_date >= TRUNC(dateInput) AND spl.create_date < TRUNC(dateInput) + 1
        AND to_timestamp(to_char(spl.create_date,'HH24:MI:SS'), 'HH24:MI:SS') > to_timestamp(to_char(to_date(cc_start, 'hh24:mi'), 'hh24:mi:ss'), 'HH24:MI:SS')
        AND to_timestamp(to_char(spl.create_date,'HH24:MI:SS'), 'HH24:MI:SS') < to_timestamp(to_char(to_date(cc_end, 'hh24:mi'), 'hh24:mi:ss'), 'HH24:MI:SS')
        AND F_GET_DISTANCE_COORDINATE(npp_lat, npp_lng, spl.lat, spl.lng) <= cc_distance
        ;
        -- Khi khong tim thay dong nay chung to Staff vi pham noi dung nay
        EXCEPTION WHEN NO_DATA_FOUND THEN nppTime := 1; rptID := null;
        -- Khi tim thay nhieu hon 1 dong thi Staff khong vi pham noi dung nay
                  WHEN TOO_MANY_ROWS THEN nppTime := 0; rptID := null;
      END;
      if rptID is not null then
        nppTime := 0;
      end if;
    else
      nppTime := 0;
    end if;

    -- Tinh cho cot FIRST_CUS_LATE ghe tham khach hang dau tien muon
    if systimestamp > to_timestamp(to_char(dateInput, 'dd/mm/yyyy') || ' ' || to_char(to_date(dt_start, 'hh24:mi'), 'hh24:mi:ss'), 'dd/mm/yyyy HH24:MI:SS') then
      BEGIN
        SELECT action_log_id INTO rptID
        FROM action_log al 
        WHERE al.staff_id = v.staff_id
        AND al.start_time >= TRUNC(dateInput) AND al.start_time < TRUNC(dateInput) + 1
        AND al.is_or = 0 AND al.object_type in (0,1) -- Khach hang trong tuyen, loai ghe tham: ghe tham, ghe tham dong cua
        AND to_timestamp(to_char(al.start_time,'HH24:MI:SS'), 'HH24:MI:SS') < to_timestamp(to_char(to_date(dt_start, 'hh24:mi'), 'hh24:mi:ss'), 'HH24:MI:SS')
        ;
        -- Khi khong tim thay dong nay chung to Staff vi pham noi dung nay
        EXCEPTION WHEN NO_DATA_FOUND THEN firstCusLate := 1; rptID := null;
        -- Khi tim thay nhieu hon 1 dong thi Staff khong vi pham noi dung nay
                  WHEN TOO_MANY_ROWS THEN firstCusLate := 0; rptID := null;
      END;
      if rptID is not null then
        firstCusLate := 0;
      end if;
    else
      firstCusLate := 0;
    end if;

    -- Tinh cho cot LAST_CUS_LATE ket thuc ghe tham khach hang cuoi cung som
    if systimestamp > to_timestamp(to_char(dateInput, 'dd/mm/yyyy') || ' ' || to_char(to_date(dt_end, 'hh24:mi'), 'hh24:mi:ss'), 'dd/mm/yyyy HH24:MI:SS') then
      BEGIN
        SELECT t.action_log_id INTO rptID
        FROM (
        SELECT action_log_id, start_time, end_time,
               RANK() OVER (PARTITION BY staff_id, trunc(start_time) ORDER BY start_time desc) rank
        FROM action_log al
        WHERE al.staff_id = v.staff_id
        AND al.start_time >= TRUNC(dateInput) AND al.start_time < TRUNC(dateInput) + 1
        AND al.is_or = 0 AND al.object_type in (0,1) -- Khach hang trong tuyen, loai ghe tham: ghe tham, ghe tham dong cua
        ) t
        WHERE t.rank = 1
        AND ((t.end_time IS NOT NULL and (to_timestamp(to_char(t.end_time,'HH24:MI:SS'), 'HH24:MI:SS') > to_timestamp(to_char(to_date(dt_end, 'hh24:mi'), 'hh24:mi:ss'), 'HH24:MI:SS')))
          OR (t.end_time IS NULL and to_timestamp(to_char(t.start_time,'HH24:MI:SS'), 'HH24:MI:SS') > to_timestamp(to_char(to_date(dt_end, 'hh24:mi'), 'hh24:mi:ss'), 'HH24:MI:SS')))
        ;
        -- Khi khong tim thay dong nay chung to Staff vi pham noi dung nay
        EXCEPTION WHEN NO_DATA_FOUND THEN lastCusEarly := 1; rptID := null;
        -- Khi tim thay nhieu hon 1 dong thi Staff khong vi pham noi dung nay
                  WHEN TOO_MANY_ROWS THEN lastCusEarly := 0; rptID := null;
      END;
      if rptID is not null then
        lastCusEarly := 0;
      end if;
    else
      lastCusEarly := 0;
    end if;

    -- Tinh cho cot ROUTING_TIME 
    if systimestamp > to_timestamp(to_char(dateInput, 'dd/mm/yyyy') || ' ' || to_char(to_date(dt_end, 'hh24:mi'), 'hh24:mi:ss'), 'dd/mm/yyyy HH24:MI:SS') then
      begin
        SELECT SUBSTR(value,1,INSTR(value, '-')-1),SUBSTR(value, INSTR(value, '-')      +1) into v_from_time_rest_str, v_to_time_rest_str
        FROM ap_param
        WHERE ap_param_code = 'REST_TIME';
      exception WHEN NO_DATA_FOUND THEN
        v_from_time_rest_str := '11:30:00';
        v_to_time_rest_str := '13:00:00';
      end;
      
      begin
        select min(start_time) into v_start_time
        from action_log
        where staff_id = v.staff_id and start_time >= TRUNC(dateInput) AND start_time < TRUNC(dateInput) + 1
          AND is_or = 0 AND object_type in (0,1);
        exception when no_data_found then routingTime := 1; goto end_check;
      end;
      
      begin
        select case when end_time is not null then end_time else start_time end into v_end_time from(
        select start_time, end_time
        from action_log
        where staff_id = v.staff_id and start_time >= TRUNC(dateInput) AND start_time < TRUNC(dateInput) + 1
          AND is_or = 0 AND object_type in (0,1)
        order by start_time desc) where rownum = 1;
        exception when no_data_found then routingTime := 1; goto end_check;
      end;
      v_from_time_rest := to_date(v_from_time_rest_str || ' ' || to_char(v_start_time,'dd/mm/yyyy'),'HH24:MI:SS DD/MM/YYYY');
      v_to_time_rest := to_date(v_to_time_rest_str || ' ' || to_char(v_start_time,'dd/mm/yyyy'),'HH24:MI:SS DD/MM/YYYY');
      if v_start_time <= v_from_time_rest and v_end_time >= v_to_time_rest then
        v_time := (v_end_time - v_start_time)*24 - (v_to_time_rest - v_from_time_rest)*24;
      elsif (v_start_time <= v_from_time_rest and v_end_time <= v_from_time_rest) or (v_start_time >= v_to_time_rest and v_end_time >= v_to_time_rest) then
        v_time := (v_end_time - v_start_time)*24;
      elsif v_start_time >= v_from_time_rest and v_end_time <= v_to_time_rest then
        v_time := 0;
      elsif v_start_time <= v_from_time_rest and v_end_time <= v_to_time_rest then
        v_time := (v_from_time_rest - v_start_time)*24;
      elsif v_start_time >= v_from_time_rest and v_start_time <= v_to_time_rest and v_end_time >= v_to_time_rest then
        v_time := (v_end_time - v_to_time_rest)*24;
      end if;
      if v_time < CONST_ROUTINGTIME then
        routingTime := 1;
      end if;
    end if;
    <<end_check>>
    checkinsert := 0;
    BEGIN
      SELECT rpt.rpt_staff_fault_id INTO rptID FROM rpt_staff_fault rpt 
      WHERE rpt.staff_id = v.staff_id AND rpt.rpt_in_date >= TRUNC(dateInput) AND rpt.rpt_in_date < TRUNC(dateInput) + 1;
      
      EXCEPTION WHEN NO_DATA_FOUND THEN checkinsert := 1;
    END;

    IF (checkinsert = 1) THEN
      INSERT INTO rpt_staff_fault (CREATE_DATE,CREATE_USER,FIRST_CUS_LATE,LAST_CUS_EARLY,NPP_TIME,ROUTING_TIME,RPT_IN_DATE,RPT_STAFF_FAULT_ID,SHOP_ID,STAFF_ID)
      VALUES (
        SYSDATE -- create_date
        ,'P_RPT_STAFF_FAULT' -- CREATE_USER
        ,firstCusLate -- FIRST_CUS_LATE
        ,lastCusEarly -- LAST_CUS_EARLY
        ,nppTime      -- NPP_TIME
        ,routingTime  -- ROUTING_TIME
        ,dateInput  -- RPT_IN_DATE
        ,RPT_STAFF_FAULT_SEQ.nextval -- RPT_STAFF_FAULT_ID
        ,v.shop_id  -- SHOP_ID
        ,v.staff_id -- STAFF_ID
        );
      DBMS_OUTPUT.PUT_LINE('INSERT staff_id = ' || v.staff_id);
    ELSE
      UPDATE rpt_staff_fault rpt
      SET FIRST_CUS_LATE = firstCusLate
          ,LAST_CUS_EARLY = lastCusEarly
          ,NPP_TIME = nppTime
          ,ROUTING_TIME = routingTime
          ,UPDATE_DATE = SYSDATE
          ,UPDATE_USER = 'P_RPT_STAFF_FAULT'
      WHERE rpt.rpt_staff_fault_id = rptID;
      DBMS_OUTPUT.PUT_LINE('UPDATE staff_id = ' || v.staff_id || ' and rpt_staff_fault_id = ' || rptID);
    END IF;
    COMMIT;
  END LOOP;
  
  FOR v IN c_DeleteData
  LOOP
    DELETE FROM rpt_staff_fault rpt WHERE rpt.rpt_staff_fault_id = v.rpt_staff_fault_id;
    DBMS_OUTPUT.PUT_LINE('DELETE rpt_staff_fault_id = ' || v.rpt_staff_fault_id);
  END LOOP;
  COMMIT;
  
  EXCEPTION
    WHEN OTHERS THEN
      BEGIN
      ROLLBACK;
      DBMS_OUTPUT.PUT_LINE('Co loi trong qua trinh xu ly');
      END;
  DBMS_OUTPUT.PUT_LINE('P_RPT_STAFF_FAULT finished');
END;

-----b. Chinh sua body bao cao CRM 3.8
PROCEDURE P_CRM_3_8(res OUT SYS_REFCURSOR, lstShopId CLOB, toDate DATE)
  -- @author: TrungNT
  -- CRM-3.8 - Báo cáo tong hop cac chi so thi truong hang ngay
  as
    fromDate date := trunc(toDate,'month');
  begin
    open res for
      with shopIdTB as
      (select shop_id from shop start with shop_id in (SELECT column_value as giaTri FROM TABLE(F_SPLIT_CLOB(lstShopId))) 
        connect by prior shop_id = parent_shop_id 
      ) 
      ,nppTB as
      (select shop_id, shop_code, short_name, parent_shop_id  
      from shop s join channel_type ct on s.shop_type_id = ct.channel_type_id 
      where s.shop_id in (
        select shop_id from shopIdTB 
      ) and ct.type = 1 and ct.object_type = 3) 
      ,nvTB as
      (select * from (
        select s.staff_id 
            ,s.staff_code 
            ,s.staff_name 
            ,sh.shop_id 
            ,case when sh.staff_owner_id is not null then sh.staff_owner_id 
            else s.staff_owner_id end staff_owner_id 
            ,row_number() over (partition by s.staff_id order by nvl(sh.to_date,sysdate) desc) rn
        from staff_history sh join staff s on sh.staff_id = s.staff_id and s.status in (0,1)
          join channel_type ct on sh.staff_type_id = ct.channel_type_id 
        where sh.istree = 1 and sh.status in (0,1) and trunc(sh.from_date) <= trunc(toDate) and (sh.to_date is null or trunc(sh.to_date) >= trunc(fromDate))
          and sh.shop_id in (select shop_id from nppTB) and ct.type = 2 and ct.object_type = 1) 
      where rn = 1 
      )
      ,rrcTB as
      (select staff_id, count(rpt_in_date) thLuyKeNCLV, sum(visited_customer) thLuyKeGheTham
      from rpt_routing_customer
      where staff_id in (select staff_id from nvTB) and shop_id in (select shop_id from nppTB)
        and rpt_in_date >= trunc(fromDate) and rpt_in_date < trunc(toDate) + 1 and visited_customer > 0
      group by staff_id
      )
      ,doTB as
      (select staff_id
          ,case when trunc(from_date) >= fromDate then trunc(from_date) else fromDate end from_date
          ,case when trunc(to_date) >= trunc(toDate) then trunc(toDate) else trunc(to_date) end to_date
          ,is_full_time 
      from day_off
      where fromDate <= trunc(to_date) and trunc(toDate) >= trunc(from_date) and status = 1 and staff_id in (select staff_id from nvTB)
      )
      ,dayOffTB as 
      (select staff_id, sum(case when is_full_time is not null and is_full_time = 0 then 0.5 else to_date - from_date + 1 end) ngayNghi
      from doTB group by staff_id
      )
      ,viPhamLuyKeTB as
      (select staff_id, sum(routing_time) viPham
      from rpt_staff_fault
      where staff_id in (select staff_id from nvTB) and shop_id in (select shop_id from nppTB)
        and rpt_in_date >= trunc(fromDate) and rpt_in_date < trunc(toDate) + 1
      group by staff_id
      )
      ,sanLuongLuyKeNgayTruocTB as
      (select rss.staff_id, sum(rss.day_quantity*rss.CONVFACT*p.VOLUMN) thLuyKeNgayTruoc 
      from rpt_sale_statistic rss join product p on rss.product_id = p.product_id
      where rss.staff_id in (select staff_id from nvTB) and rss.shop_id in (select shop_id from nppTB)
        and rpt_in_date >= trunc(fromDate) and rpt_in_date < trunc(toDate)
      group by rss.staff_id
      )
      ,sanLuongNgayTB as
      (select rss.staff_id, sum(rss.day_quantity*rss.CONVFACT*p.VOLUMN) thLuyKeNgay
      from rpt_sale_statistic rss join product p on rss.product_id = p.product_id
      where rss.staff_id in (select staff_id from nvTB) and rss.shop_id in (select shop_id from nppTB)
        and rpt_in_date >= trunc(toDate) and rpt_in_date < trunc(toDate) + 1
      group by rss.staff_id
      )
      ,soLuongDonHangNgayTB as
      (select staff_id, sum(success_order) thNgay
      from rpt_sale_order
      where staff_id in (select staff_id from nvTB) and shop_id in (select shop_id from nppTB)
        and rpt_in_date >= trunc(toDate) and rpt_in_date < trunc(toDate) + 1
      group by staff_id
      )
      ,soLuongDonHangLuyKeNgayTruocTB as
      (select staff_id, sum(success_order) thLuyKeNgayTruoc
      from rpt_sale_order
      where staff_id in (select staff_id from nvTB) and shop_id in (select shop_id from nppTB)
        and rpt_in_date >= trunc(fromDate) and rpt_in_date < trunc(toDate)
      group by staff_id
      )
      ,pdpTB as
      (select distinct pdp.staff_id, pdp.customer_id
      from col_product_volume pdp join customer cus on pdp.customer_id = cus.customer_id
        join col_product_volume_media cpvm on pdp.col_product_volume_id = cpvm.col_product_volume_id
          and cpvm.create_date >= trunc(fromDate) and cpvm.create_date < trunc(toDate)
          and cpvm.type = 1 and cpvm.status = 1
      where pdp.staff_id in (select staff_id from nvTB) and cus.shop_id in (select shop_id from nppTB)
        and pdp.collect_date >= trunc(fromDate) and pdp.collect_date < trunc(fromDate) + 1 and pdp.type = 1
      )
      ,soTrungBayLuyKeNgayTruocTB as
      (select pdp.staff_id, count(pdp.customer_id) thLuyKeNgayTruoc
      from pdpTB pdp
      group by pdp.staff_id
      )
      ,soTrungBayNgayTB as
      (select pdp.staff_id, count(distinct pdp.customer_id) thNgay
      from col_product_volume pdp join customer cus on pdp.customer_id = cus.customer_id
        join col_product_volume_media cpvm on pdp.col_product_volume_id = cpvm.col_product_volume_id
          and cpvm.create_date >= trunc(toDate) and cpvm.create_date < trunc(toDate) + 1
          and cpvm.type = 1 and cpvm.status = 1
      where pdp.staff_id in (select staff_id from nvTB) and cus.shop_id in (select shop_id from nppTB)
        and pdp.collect_date >= trunc(fromDate) and pdp.collect_date < trunc(fromDate) + 1 and pdp.type = 1
        and pdp.customer_id not in (select customer_id from pdpTB)
      group by pdp.staff_id
      )
      ,result as
      (select kv.short_name KhuVuc
          ,v.short_name Vung 
          ,case when gsbh.staff_code is not null and gsbh.staff_code <> ' ' then 
            gsbh.staff_code || ' - ' || gsbh.staff_name 
          else null end GSBH
          ,nv.staff_code maNVKD
          ,nv.staff_name NVKD
          ,sum(qd.plan_quantity) NCLVQuiDinh
          ,sum(do.ngayNghi) NCLVSoNgayDangKyNghi
          ,sum(rrc.thLuyKeNCLV) NCLVTHLuyKe
          ,case 
            when nvl(sum(qd.plan_quantity),0) = 0 and nvl(sum(rrc.thLuyKeNCLV),0) = 0 then null
            when nvl(sum(qd.plan_quantity),0) = 0 and nvl(sum(rrc.thLuyKeNCLV),0) > 0 then 1
            else round(sum(rrc.thLuyKeNCLV)/nvl(sum(qd.plan_quantity),0),2) end THQuyDinhNCLV
          ,sum(rsf.routing_time) SoLanViPhamNgay
          ,sum(vplk.viPham) SoLanViPhamLuyKe
          ,case 
            when sum(slbhKHThang.plan_quantity) is not null and sum(slbhKHThang.plan_quantity) > 0 then sum(slbhKHThang.plan_quantity)
            else null end SanLuongBanHangKHThang
          ,case
            when nvl(sum(slbhKHThang.plan_quantity),0) - nvl(sum(slbhNgayTruoc.thLuyKeNgayTruoc),0) > 0 
              then round((nvl(sum(slbhKHThang.plan_quantity),0) - nvl(sum(slbhNgayTruoc.thLuyKeNgayTruoc),0)) / (trunc(last_day(toDate)) - trunc(toDate) + 1),2)
              else null end SanLuongBanHangKHNgay
          ,case 
            when nvl(sum(slbhNgay.thLuyKeNgay),0) > 0 then round(nvl(sum(slbhNgay.thLuyKeNgay),0),2)
            else null end SanLuongBanHangTHNgay
          ,case
            when nvl(sum(slbhKHThang.plan_quantity),0) - nvl(sum(slbhNgayTruoc.thLuyKeNgayTruoc),0) <= 0
              and nvl(sum(slbhNgay.thLuyKeNgay),0) = 0 then null
            when nvl(sum(slbhKHThang.plan_quantity),0) - nvl(sum(slbhNgayTruoc.thLuyKeNgayTruoc),0) <= 0
              and nvl(sum(slbhNgay.thLuyKeNgay),0) > 0 then 1
            else round(nvl(sum(slbhNgay.thLuyKeNgay),0)/((nvl(sum(slbhKHThang.plan_quantity),0) - nvl(sum(slbhNgayTruoc.thLuyKeNgayTruoc),0))/(trunc(last_day(toDate)) - trunc(toDate) + 1)),2)
          end SanLuongBanHangTHKHNgay
          ,case
            when nvl(sum(slbhNgay.thLuyKeNgay),0) = 0 and nvl(sum(slbhNgayTruoc.thLuyKeNgayTruoc),0) = 0 then null
            else (nvl(sum(slbhNgay.thLuyKeNgay),0)+nvl(sum(slbhNgayTruoc.thLuyKeNgayTruoc),0)) end SanLuongBanHangTHLuyKe
          ,case
            when nvl(sum(slbhKHThang.plan_quantity),0) = 0 and (nvl(sum(slbhNgay.thLuyKeNgay),0)+nvl(sum(slbhNgayTruoc.thLuyKeNgayTruoc),0)) = 0
              then null
            when nvl(sum(slbhKHThang.plan_quantity),0) = 0 and (nvl(sum(slbhNgay.thLuyKeNgay),0)+nvl(sum(slbhNgayTruoc.thLuyKeNgayTruoc),0)) > 0
              then 1
            else round((nvl(sum(slbhNgay.thLuyKeNgay),0)+nvl(sum(slbhNgayTruoc.thLuyKeNgayTruoc),0)) / sum(slbhKHThang.plan_quantity),2)
          end SanLuongBanHangTHKHThang
          ,case
            when trunc(last_day(toDate)) - trunc(toDate) = 0 and sum(slbhKHThang.plan_quantity) - (nvl(sum(slbhNgay.thLuyKeNgay),0)+nvl(sum(slbhNgayTruoc.thLuyKeNgayTruoc),0)) > 0
              then round((sum(slbhKHThang.plan_quantity) - (nvl(sum(slbhNgay.thLuyKeNgay),0)+nvl(sum(slbhNgayTruoc.thLuyKeNgayTruoc),0))),2)
            when trunc(last_day(toDate)) - trunc(toDate) = 0 and sum(slbhKHThang.plan_quantity) - (nvl(sum(slbhNgay.thLuyKeNgay),0)+nvl(sum(slbhNgayTruoc.thLuyKeNgayTruoc),0)) <= 0
              then 0
            when sum(slbhKHThang.plan_quantity) - (nvl(sum(slbhNgay.thLuyKeNgay),0)+nvl(sum(slbhNgayTruoc.thLuyKeNgayTruoc),0)) > 0
             then round((sum(slbhKHThang.plan_quantity) - (nvl(sum(slbhNgay.thLuyKeNgay),0)+nvl(sum(slbhNgayTruoc.thLuyKeNgayTruoc),0)))/(trunc(last_day(toDate)) - trunc(toDate)),2)
            else null end SanLuongBanHangKeHoachConLai
          ,case 
            when nvl(sum(slgtNgay.total_customer),0) > 0 then sum(slgtNgay.total_customer)
            else null end SoLuongDiemBanKHNgay
          ,case
            when nvl(sum(slgtNgay.visited_customer),0) > 0 then sum(slgtNgay.visited_customer)
            else null end SoLuongDiemBanTHNgay
          ,case
            when nvl(sum(slgtNgay.total_customer),0) = 0 and nvl(sum(slgtNgay.visited_customer),0) = 0 then null
            when nvl(sum(slgtNgay.total_customer),0) = 0 and nvl(sum(slgtNgay.visited_customer),0) > 0 then 1
            else round(nvl(sum(slgtNgay.visited_customer),0)/nvl(sum(slgtNgay.total_customer),0),2) end SoLuongDiemBanTHKHNgay
          ,sum(rrc.thLuyKeGheTham) SoLuongDiemBanBQTHNgayLuyKe
          ,case
            when nvl(sum(slgtKHThang.plan_quantity),0) = 0 and nvl(sum(rrc.thLuyKeGheTham),0) = 0 then null
            when nvl(sum(slgtKHThang.plan_quantity),0) = 0 and nvl(sum(rrc.thLuyKeGheTham),0) > 0 then 1
            else round((nvl(sum(rrc.thLuyKeGheTham),0)/sum(slgtKHThang.plan_quantity)),2) end SoLuongDiemBanBQTHLuyKeKH
          ,case
            when nvl(sum(sldhKHThang.plan_quantity),0) - nvl(sum(sldhNgayTruoc.thLuyKeNgayTruoc),0) <= 0 then 0
            else round((nvl(sum(sldhKHThang.plan_quantity),0) - nvl(sum(sldhNgayTruoc.thLuyKeNgayTruoc),0))/(trunc(last_day(toDate)) - trunc(toDate) + 1),2)
          end SoLuongDonHangKHNgay
          ,case 
            when nvl(sum(sldhNgay.thNgay),0) >0 then sum(sldhNgay.thNgay)
            else null end SoLuongDonHangTHNgay
          ,case
            when nvl(sum(sldhKHThang.plan_quantity),0) - nvl(sum(sldhNgayTruoc.thLuyKeNgayTruoc),0) <= 0
              and nvl(sum(sldhNgay.thNgay),0) = 0 then null
            when nvl(sum(sldhKHThang.plan_quantity),0) - nvl(sum(sldhNgayTruoc.thLuyKeNgayTruoc),0) <= 0
              and nvl(sum(sldhNgay.thNgay),0) > 0 then 1
            else round(nvl(sum(sldhNgay.thNgay),0)/round(((nvl(sum(sldhKHThang.plan_quantity),0) - nvl(sum(sldhNgayTruoc.thLuyKeNgayTruoc),0))/(trunc(last_day(toDate)) - trunc(toDate) + 1)),2),2)
          end SoLuongDonHangTHKHNgay
          ,case
            when nvl(sum(sldhNgay.thNgay),0) = 0 and nvl(sum(sldhNgayTruoc.thLuyKeNgayTruoc),0) = 0 then null
            else (nvl(sum(sldhNgay.thNgay),0)+nvl(sum(sldhNgayTruoc.thLuyKeNgayTruoc),0)) end SoLuongDonHangBQTHNgayLuyKe
          ,case
            when nvl(sum(sldhKHThang.plan_quantity),0) = 0 and (nvl(sum(sldhNgay.thNgay),0)+nvl(sum(sldhNgayTruoc.thLuyKeNgayTruoc),0)) = 0
              then null
            when nvl(sum(sldhKHThang.plan_quantity),0) = 0 and (nvl(sum(sldhNgay.thNgay),0)+nvl(sum(sldhNgayTruoc.thLuyKeNgayTruoc),0)) > 0
              then 1
            else round((nvl(sum(sldhNgay.thNgay),0)+nvl(sum(sldhNgayTruoc.thLuyKeNgayTruoc),0)) / sum(sldhKHThang.plan_quantity),2)
          end SoLuongDonHangBQTHLuyKeKH
          ,case
            when nvl(sum(sldhKHThang.plan_quantity),0) - nvl(sum(sldhNgayTruoc.thLuyKeNgayTruoc),0) <= 0 or
            ((nvl(sum(slbhKHThang.plan_quantity),0) - nvl(sum(slbhNgayTruoc.thLuyKeNgayTruoc),0))/(trunc(last_day(toDate)) - trunc(toDate) + 1))
              /((nvl(sum(sldhKHThang.plan_quantity),0) - nvl(sum(sldhNgayTruoc.thLuyKeNgayTruoc),0))/(trunc(last_day(toDate)) - trunc(toDate) + 1)) <= 0 then null
            else round(round(((nvl(sum(slbhKHThang.plan_quantity),0) - nvl(sum(slbhNgayTruoc.thLuyKeNgayTruoc),0))/(trunc(last_day(toDate)) - trunc(toDate) + 1)),2)
              /round(((nvl(sum(sldhKHThang.plan_quantity),0) - nvl(sum(sldhNgayTruoc.thLuyKeNgayTruoc),0))/(trunc(last_day(toDate)) - trunc(toDate) + 1)),2),2)
          end GiaTriBinhQuanDonHangKHNgay
          ,case
            when nvl(sum(sldhNgay.thNgay),0) = 0 then null
            else round(nvl(sum(slbhNgay.thLuyKeNgay),0)/sum(sldhNgay.thNgay),2) end GiaTriBinhQuanDonHangTHNgay
          ,case
            when nvl(sum(sldhNgay.thNgay),0) = 0 or 
            ((nvl(sum(sldhKHThang.plan_quantity),0) - nvl(sum(sldhNgayTruoc.thLuyKeNgayTruoc),0) <= 0 or
            ((nvl(sum(slbhKHThang.plan_quantity),0) - nvl(sum(slbhNgayTruoc.thLuyKeNgayTruoc),0))/(trunc(last_day(toDate)) - trunc(toDate) + 1))
              /((nvl(sum(sldhKHThang.plan_quantity),0) - nvl(sum(sldhNgayTruoc.thLuyKeNgayTruoc),0))/(trunc(last_day(toDate)) - trunc(toDate) + 1)) <= 0)
              and nvl(sum(slbhNgay.thLuyKeNgay),0)/sum(sldhNgay.thNgay) = 0) then null
            when (nvl(sum(sldhKHThang.plan_quantity),0) - nvl(sum(sldhNgayTruoc.thLuyKeNgayTruoc),0) <= 0 or
            ((nvl(sum(slbhKHThang.plan_quantity),0) - nvl(sum(slbhNgayTruoc.thLuyKeNgayTruoc),0))/(trunc(last_day(toDate)) - trunc(toDate) + 1))
              /((nvl(sum(sldhKHThang.plan_quantity),0) - nvl(sum(sldhNgayTruoc.thLuyKeNgayTruoc),0))/(trunc(last_day(toDate)) - trunc(toDate) + 1)) <= 0)
              and nvl(sum(slbhNgay.thLuyKeNgay),0)/sum(sldhNgay.thNgay) > 0 then 1
            else round((nvl(sum(slbhNgay.thLuyKeNgay),0)/sum(sldhNgay.thNgay))/
              (((nvl(sum(slbhKHThang.plan_quantity),0) - nvl(sum(slbhNgayTruoc.thLuyKeNgayTruoc),0))/(trunc(last_day(toDate)) - trunc(toDate) + 1))
              /((nvl(sum(sldhKHThang.plan_quantity),0) - nvl(sum(sldhNgayTruoc.thLuyKeNgayTruoc),0))/(trunc(last_day(toDate)) - trunc(toDate) + 1))),2)
          end GiaTriBinhQuanDonHangTHKH
          ,case
            when nvl(sum(sldhNgay.thNgay),0) = 0 and nvl(sum(sldhNgayTruoc.thLuyKeNgayTruoc),0) = 0 then null
            else round((nvl(sum(slbhNgay.thLuyKeNgay),0)+nvl(sum(slbhNgayTruoc.thLuyKeNgayTruoc),0))/(nvl(sum(sldhNgay.thNgay),0)+nvl(sum(sldhNgayTruoc.thLuyKeNgayTruoc),0)),2)
          end GiaTriBinhQuanDonHangTHLuyKe
          ,case
            when nvl(sum(stbKHThang.plan_quantity),0) - nvl(sum(stbNgayTruoc.thLuyKeNgayTruoc),0) <= 0 then null
            else round((nvl(sum(stbKHThang.plan_quantity),0) - nvl(sum(stbNgayTruoc.thLuyKeNgayTruoc),0))/(trunc(last_day(toDate)) - trunc(toDate) + 1),2)
          end SoDiemBanTrungBayKHNgay
          ,sum(stbNgay.thNgay) SoDiemBanTrungBayTHNgay
          ,case
            when nvl(sum(stbKHThang.plan_quantity),0) - nvl(sum(stbNgayTruoc.thLuyKeNgayTruoc),0) <= 0 
              and nvl(sum(stbNgay.thNgay),0) = 0 then null
            when nvl(sum(stbKHThang.plan_quantity),0) - nvl(sum(stbNgayTruoc.thLuyKeNgayTruoc),0) <= 0 
              and nvl(sum(stbNgay.thNgay),0) > 0 then 1
            else round(nvl(sum(stbNgay.thNgay),0)/((nvl(sum(stbKHThang.plan_quantity),0) - nvl(sum(stbNgayTruoc.thLuyKeNgayTruoc),0))/(trunc(last_day(toDate)) - trunc(toDate) + 1)),2)
          end SoDiemBanTrungBayTHKHNgay
          ,case 
            when nvl(sum(stbNgayTruoc.thLuyKeNgayTruoc),0)+nvl(sum(stbNgay.thNgay),0) > 0 then nvl(sum(stbNgayTruoc.thLuyKeNgayTruoc),0)+nvl(sum(stbNgay.thNgay),0)
            else null end SoDiemBanTrungBayBQTHNgayLuyKe
          ,case
            when nvl(sum(stbKHThang.plan_quantity),0) = 0 then null
            else round((nvl(sum(stbNgayTruoc.thLuyKeNgayTruoc),0)+nvl(sum(stbNgay.thNgay),0))/nvl(sum(stbKHThang.plan_quantity),0),2)
          end SoDiemBanTrungBayBQTHLuyKeKH
          ,GROUPING_ID (kv.shop_code,v.shop_code,gsbh.staff_code,nv.staff_code) tmp
      from nppTB join shop v on nppTB.parent_shop_id = v.shop_id 
        join shop kv on v.parent_shop_id = kv.shop_id 
        join nvTB nv on nv.shop_id = nppTB.shop_id 
        left join staff gsbh on gsbh.staff_id = nv.staff_owner_id and gsbh.status in (0,1)
        left join kpi_result_detail qd on qd.object_type = 2 and qd.object_id = nv.staff_id and qd.kpi_id = 14
          and qd.month = extract(month from toDate) and qd.year = extract(year from toDate)
        left join rrcTB rrc on rrc.staff_id = nv.staff_id
        left join dayOffTB do on do.staff_id = nv.staff_id
        left join rpt_staff_fault rsf on rsf.staff_id = nv.staff_id and rsf.rpt_in_date >= trunc(toDate)
          and rsf.rpt_in_date < trunc(toDate) + 1
        left join viPhamLuyKeTB vplk on vplk.staff_id = nv.staff_id
        left join kpi_result_detail slbhKHThang on slbhKHThang.object_type = 2 and slbhKHThang.object_id = nv.staff_id
          and slbhKHThang.kpi_id = 1 and slbhKHThang.month = extract(month from toDate) and slbhKHThang.year = extract(year from toDate)
        left join sanLuongLuyKeNgayTruocTB slbhNgayTruoc on slbhNgayTruoc.staff_id = nv.staff_id
        left join sanLuongNgayTB slbhNgay on slbhNgay.staff_id = nv.staff_id
        left join rpt_routing_customer slgtNgay on slgtNgay.staff_id = nv.staff_id and slgtNgay.rpt_in_date >= trunc(toDate)
          and slgtNgay.rpt_in_date < trunc(toDate) + 1
        left join kpi_result_detail slgtKHThang on slgtKHThang.object_type = 2 and slgtKHThang.object_id = nv.staff_id 
          and slgtKHThang.kpi_id = 3 and slgtKHThang.month = extract(month from toDate) and slgtKHThang.year = extract(year from toDate)
        left join kpi_result_detail sldhKHThang on sldhKHThang.object_type = 2 and sldhKHThang.object_id = nv.staff_id 
          and sldhKHThang.kpi_id = 15 and sldhKHThang.month = extract(month from toDate) and sldhKHThang.year = extract(year from toDate)
        left join soLuongDonHangLuyKeNgayTruocTB sldhNgayTruoc on sldhNgayTruoc.staff_id = nv.staff_id
        left join soLuongDonHangNgayTB sldhNgay on sldhNgay.staff_id = nv.staff_id
        left join kpi_result_detail stbKHThang on stbKHThang.object_type = 2 and stbKHThang.object_id = nv.staff_id 
          and stbKHThang.kpi_id = 16 and stbKHThang.month = extract(month from toDate) and stbKHThang.year = extract(year from toDate)
        left join soTrungBayLuyKeNgayTruocTB stbNgayTruoc on stbNgayTruoc.staff_id = nv.staff_id
        left join soTrungBayNgayTB stbNgay on stbNgay.staff_id = nv.staff_id
      group by rollup((kv.shop_code,kv.short_name),(v.shop_code,v.short_name),(gsbh.staff_code,gsbh.staff_name),(nv.staff_code,nv.staff_name))
      )
      select case 
              when tmp = 15 then 'Tổng' || KhuVuc
              when tmp = 7 then 'Tổng ' || KhuVuc
              else KhuVuc end KhuVuc
            ,case when tmp = 3 then 'Tổng ' || Vung else Vung end Vung
            ,case when tmp = 1 then 'Tổng ' || GSBH else GSBH end GSBH
            ,maNVKD
            ,NVKD
            ,NCLVQuiDinh
            ,NCLVSoNgayDangKyNghi
            ,NCLVTHLuyKe
            ,THQuyDinhNCLV
            ,SoLanViPhamNgay
            ,SoLanViPhamLuyKe
            ,SanLuongBanHangKHThang
            ,SanLuongBanHangKHNgay
            ,SanLuongBanHangTHNgay
            ,SanLuongBanHangTHKHNgay
            ,SanLuongBanHangTHLuyKe
            ,SanLuongBanHangTHKHThang
            ,SanLuongBanHangKeHoachConLai
            ,SoLuongDiemBanKHNgay
            ,SoLuongDiemBanTHNgay
            ,SoLuongDiemBanTHKHNgay
            ,SoLuongDiemBanBQTHNgayLuyKe
            ,SoLuongDiemBanBQTHLuyKeKH
            ,SoLuongDonHangKHNgay
            ,SoLuongDonHangTHNgay
            ,SoLuongDonHangTHKHNgay
            ,SoLuongDonHangBQTHNgayLuyKe
            ,SoLuongDonHangBQTHLuyKeKH
            ,GiaTriBinhQuanDonHangKHNgay
            ,GiaTriBinhQuanDonHangTHNgay
            ,GiaTriBinhQuanDonHangTHKH
            ,GiaTriBinhQuanDonHangTHLuyKe
            ,SoDiemBanTrungBayKHNgay
            ,SoDiemBanTrungBayTHNgay
            ,SoDiemBanTrungBayTHKHNgay
            ,SoDiemBanTrungBayBQTHNgayLuyKe
            ,SoDiemBanTrungBayBQTHLuyKeKH
            ,tmp
      from result;
  end P_CRM_3_8;

---III. Rollback
----1. Chay script o file 'BackUpCRMBody.sql'
---------------------------------------------
----2. Chay script o file 'BackUp_P_RPT_STAFF_FAULT.sql'